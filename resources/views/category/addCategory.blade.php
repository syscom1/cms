@extends('layouts.app', ['activePage' => 'listCategory', 'titlePage' => __('Add Services Categories')])


@section('content')

<div class="content">
@if($s == '1')
    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
@endif

@if($s == '0')
    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
@endif
</br>


    <div class="container-fluid">
        <div class="card">
        <div class="card-header card-header-text card-header-success text-right">
        <div class="card-text ">
              <h4 class="card-title">Add New Services Categories</h4>
            </div>
            </div>
            <div class="card-body">

            <div class="col-md-12">
                <form method="post" action="{{ route('category.addNewCategory') }}" autocomplete="off"  enctype="multipart/form-data">
                    @csrf
                    <div class="pl-lg-4">


                            <label class="form-control-label" style="font-size: 20px">TOP</label><br>

                            <img id="output" width="50%"/>
                            <div class="form-group">
                            <label class="form-control-label" for="input-name" >Image </label>
                            </div>
                            <div class="form-group">
                            <label for="exampleInputFile" class="bmd-label-floating">
                            <p  class="btn btn-success mt-4">
                            Click to add image</p>
                            </label>
                            <input type="file" class="form-control-file" name="banner_img"  id="exampleInputFile" onchange="loadFile(event)"  />
                            </div> 

                            <div class="form-group{{ $errors->has('banner_title') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Title</label>
                            <br>
                            <input type="text" name="banner_title" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('banner_title') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('banner_title') }}" required autofocus>
                            </div>

                            <div class="form-group{{ $errors->has('banner_link') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-name">Link</label>
                                <br>
                                <input type="text" name="banner_link" id="input-name"
                                    class="form-control  form-control-alternative{{ $errors->has('banner_link') ? ' is-invalid' : '' }}"
                                    placeholder="{{ __(' ') }}" value="{{ old('banner_link') }}" required autofocus>
                                </div>
                                <hr />

                                <label class="form-control-label" style="font-size: 20px">Cover</label><br>

                            <img id="output1" width="50%"/>
                            <div class="form-group">
                            <label class="form-control-label" for="input-name" >Image </label>
                            </div>
                            <div class="form-group">
                            <label for="exampleInputFile1" class="bmd-label-floating">
                            <p  class="btn btn-success mt-4">
                            Click to add image</p>
                            </label>
                            <input type="file" class="form-control-file" name="cover_img"  id="exampleInputFile1" onchange="loadFile1(event)"  />
                            </div> 

                            <div class="form-group{{ $errors->has('banner_title') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Title</label>
                            <br>
                            <input type="text" name="cover_title" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('banner_title') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('banner_title') }}" autofocus>
                            </div>
                        
                            <div class="form-group">
                            <label style="right: 0;" class="right">Category details</label>
                            <textarea name="cover_text" id="editor1" class="editor" rows="10" cols="80" required>
                            </textarea>
 
                            </div>

               </div>

                        
                        <div class="text-center">
                            <a href="{{ route('category.listCategory') }}"  class="btn btn-primary mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>

@endsection

@push('js')

<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };
  var loadFile1 = function(event) {
    var output = document.getElementById('output1');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };
</script>

<script>
 CKEDITOR.replace( 'editor' );
 CKEDITOR.replace( 'editor1' );
 CKEDITOR.replace( 'editor2' );

</script>
    @endpush