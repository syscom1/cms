@extends('layouts.app', ['activePage' => 'listCategory', 'titlePage' => __('Services Categories List')])


@section('content')
</br>
</br>
</br>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="card-header">
                            <div class="row align-items-center">
                               
                                <div class="col-sm-8">
                                    <h3 class="mb-0">{{ __('Services Categories') }}</h3>

                                </div>


                                <div class="col-sm-4">
                                <div class="col-md-4 offset-md-4 clearfix">
                                    <a href="{{ route('category.addCategory') }}" class="btn btn-primary btn-lg " role="button">{{ __('Add Services Cat') }}</a>
                                </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            
        @if (count($errors) > 0)
        @if($errors->any())
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{$errors->first()}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif
    @endif
                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                        </div>



                        <div class="table-responsive">
                            <table class="table align-items-center table-flush datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">{{ __('#') }}</th>
                                        <th scope="col">{{ __('Category Name') }}</th>
                                        <th scope="col">{{ __('Banner Link') }}</th>
                                        <th scope="col">{{ __('Banner Photo') }}</th>
                                        <th scope="col">{{ __('Cover Title') }}</th>
                                        <th scope="col">{{ __('Cover Text') }}</th>
                                        <th scope="col">{{ __('Cover Photo') }}</th>
                                        <th scope="col">{{ __('Action') }}</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($listCategory as $key=>$user)
                                        <tr>
                                            <td>{{ $key+1}} </td>
                                            <td>{{ $user->name}} </td>
                                            <td>{!!html_entity_decode($user->banner_link)!!} </td>
                                            <td><button id="click{{ $user->id }}" class="btn btn-primary">View</button>
                                                @push('js')
                                                <script>
                                                   $('#click{{ $user->id }}').on('click',function(){
                                                       swal.fire({
                                                        imageUrl: '{{ asset($user->banner_photo) }}',
                                                        imageHeight: 200,
                                                        imageAlt: 'A tall image'
                                                       })
                                                   })
                                                </script>
                                            @endpush
                                            </td>
                                            <td>{{ $user->cover_title}} </td>
                                            <td>{!! html_entity_decode($user->cover_text) !!} </td>
                                            <td>@if ($user->cover_photo)
                                                <button id="click1{{ $user->id }}" class="btn btn-primary">View</button>
                                                @push('js')
                                                <script>
                                                   $('#click1{{ $user->id }}').on('click',function(){
                                                       swal.fire({
                                                        imageUrl: '{{ asset($user->cover_photo) }}',
                                                        imageHeight: 200,
                                                        imageAlt: 'A tall image'
                                                       })
                                                   })
                                                </script>
                                            @endpush
                                            @endif
                                               
                                            </td>
                                            <td >
                                            <a href="deleteCategory/{{$user->category_id}}" 
                                               onclick="return confirm('Are you sure you want to delete this category?')"   class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                                <i class="material-icons">delete</i></a>

                                            @if($user->category_status == 0)
                                            <a href="statusCategory/{{$user->category_id}}/1"  class="btn btn-primary btn-fab btn-fab-mini btn-round"
                                            onclick="return confirm('Are you sure you want to restore this Category?')"                                             class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                             <i class="material-icons">refresh</i></a>
                                                @endif
                                                @if($user->category_status == 1)

                                             <a href="statusCategory/{{$user->category_id}}/0" 
                                            onclick="return confirm('Are you sure you want to hide this Category?')"                                             class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                             <i class="material-icons">close</i></a>
                                             @endif

                                            <a href="editCategory/{{$user->category_id}}" class="btn btn-success btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                             <i class="material-icons">edit</i></a>
                                             <a href="addImgCat/{{$user->category_id}}" class="btn btn-success btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                                <i class="material-icons">photo</i></a>


                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>



                       
            
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('js')
    <script>
$('.datatable').DataTable();

    </script>
    @endpush