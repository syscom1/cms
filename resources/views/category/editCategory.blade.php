@extends('layouts.app', [
'class' => '',
'activePage' => 'listCategory', 'titlePage' => __('Edit Services Categories')])

@section('content')
<div class="content">

@if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
</br>

    <div class="container-fluid">
    <div class="card-header">

        <div class="row">

            <div class="col-md-12">
                <form method="post" action="{{ route('category.updateCategory') }}" autocomplete="off"  enctype="multipart/form-data">
                    @csrf
                    <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="category_id" id="input-id" type="text"   value="{{ old('id', $editCategory->category_id) }}" hidden/>
                    <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="category_image_old" id="input-id" type="hidden"   value="{{ old('id', $editCategory->category_image) }}" hidden/>

                   
                     
                    <div class="pl-lg-4">

                        <label class="form-control-label" style="font-size: 20px">TOP</label><br>

                        <img id="output" src="{{asset('').$editCategory->banner_photo}}" width="50%"/>
                        <div class="form-group">
                        <label class="form-control-label" for="input-name" >Image </label>
                        </div>
                        <div class="form-group">
                        <label for="exampleInputFile" class="bmd-label-floating">
                        <p  class="btn btn-success mt-4">
                        Click to add image</p>
                        </label>
                        <input type="file" class="form-control-file" name="banner_img"  id="exampleInputFile" onchange="loadFile(event)"  />
                        </div> 

                        <div class="form-group{{ $errors->has('banner_title') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-name">Title</label>
                        <br>
                        <input type="text" name="banner_title" id="input-name"
                            class="form-control  form-control-alternative{{ $errors->has('banner_title') ? ' is-invalid' : '' }}"
                            placeholder="{{ __(' ') }}" value="{{ $editCategory->name }}" required autofocus>
                        </div>

                        <div class="form-group{{ $errors->has('banner_link') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Link</label>
                            <br>
                            <input type="text" name="banner_link" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('banner_link') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ $editCategory->banner_link }}" required autofocus>
                            </div>
                            <hr />

                            <label class="form-control-label" style="font-size: 20px">Cover</label><br>

                        <img id="output1" @if($editCategory->cover_photo) src="{{asset('').$editCategory->cover_photo}}" @endif width="50%"/>
                        <div class="form-group">
                        <label class="form-control-label" for="input-name" >Image </label>
                        </div>
                        <div class="form-group">
                        <label for="exampleInputFile1" class="bmd-label-floating">
                        <p  class="btn btn-success mt-4">
                        Click to add image</p>
                        </label>
                        <input type="file" class="form-control-file" name="cover_img"  id="exampleInputFile1" onchange="loadFile1(event)"  />
                        </div> 

                        <div class="form-group{{ $errors->has('banner_title') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-name">Title</label>
                        <br>
                        <input type="text" name="cover_title" id="input-name"
                            class="form-control  form-control-alternative{{ $errors->has('banner_title') ? ' is-invalid' : '' }}"
                            placeholder="{{ __(' ') }}" value="{{ $editCategory->cover_title }}" autofocus>
                        </div>
                    
                        <div class="form-group">
                        <label style="right: 0;" class="right">Category details</label>
                        <textarea name="cover_text" id="editor1" class="editor" rows="10" cols="80" required>{{ $editCategory->cover_text }}</textarea>

                        </div>

               

                        <!-- <div class="form-group">
            <img  id="output" src="{{asset('').$editCategory->category_image}}" width="50%"/>

                        </div>
                          <div class="form-group">

    <label for="exampleInputFile" class="bmd-label-floating">
                                    <p  class="btn btn-success mt-4">
        Click to add image</p>
</label>
    <input type="file" class="form-control-file" name="category_image"  id="exampleInputFile" onchange="loadFile(event)"  />
   </div> -->
                        

                        <div class="text-center">
                            <a href="{{ route('category.listCategory') }}"  class="btn btn-warning mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>
@endsection

@push('js')
<script>
    var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
      }
    };
    var loadFile1 = function(event) {
      var output = document.getElementById('output1');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
      }
    };
  </script>

<script>
 CKEDITOR.replace( 'editor' );
 CKEDITOR.replace( 'editor1' );
 CKEDITOR.replace( 'editor2' );

</script>
    @endpush
