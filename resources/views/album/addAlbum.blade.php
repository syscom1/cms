@extends('layouts.app', ['activePage' => 'addAlbum', 'titlePage' => __('Add Album')])


@section('content')

<div class="content">
    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
</br>


    <div class="container-fluid">
    <div class="card">
        <div class="card-header card-header-text card-header-success text-right">
        <div class="card-text ">
              <h4 class="card-title">Add New Album</h4>
            </div>
            </div>
            <div class="card-body">
            
            <div class="col-md-12">
                <form method="post" action="{{ route('album.addNewAlbum') }}"    enctype="multipart/form-data">
                    @csrf
                    <div class="pl-lg-1">

                    

                       

                        <!-- <div class="form-group{{ $errors->has('album_title') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Title </label>
                            <br>
                            <input type="text" name="album_title" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('album_title') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('album_title') }}" required autofocus>

                        </div> -->

                        

                        <div class="form-group{{ $errors->has('album_position') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">album_position </label>
                            <br>
                            <input type="text" name="album_position" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('album_position') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('album_position') }}" required autofocus>

                        </div>
                    
    <div class="form-group">
            <img id="output" width="100%"/>

                        </div>

    <div class="form-group">

    <label for="exampleInputFile" class="bmd-label-floating">
                                    <p  class="btn btn-success mt-4">
        Click to add image</p>
</label>
    <input type="file" class="form-control-file" name="album_image" id="exampleInputFile" onchange="loadFile(event)" required/>
   </div>
  

            <div class="text-center">
                            <a href="{{ route('album.listAlbum') }}"  class="btn btn-primary mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>

@endsection

@push('js')

    <script>


$('.datetimepicker').datetimepicker({
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
    },
    format : "DD-MM-YYYY"
});
// $lastupdated = date('Y-m-d H:i:s'); dd DD-MM-YYYY YYYY-MM-DD hh:mm:ss     format : {"YYYY-MM-DD"}


    </script>
<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };
</script>
    @endpush

