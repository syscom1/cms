@extends('layouts.app', ['activePage' => 'editInfo1', 'titlePage' => 'About Home'])


@section('content')

<div class="content">
 
    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif 
</br>


    <div class="container-fluid">
        <div class="card">
        <div class="card-header card-header-text card-header-success text-right">
        <div class="card-text ">
              <h4 class="card-title">Update Info</h4>
            </div>
            </div>
            <div class="card-body">

            <div class="col-md-12">
                <form method="post" action="{{ route('infos1.updateInfos1') }}" autocomplete="off"  enctype="multipart/form-data"  class="needs-validation" novalidate>
                    @csrf
                 <input type="hidden" name="id" value="{{$editInfo1->info1_id}}">
 
                    <div class="pl-lg-4">
                        
                    

                        
                        
                        
                          <div class="form-group">
             <label style="right: 0;" class="right">ROI</label>

     <textarea name="info1_about_us" id="editor" class="editor" rows="10" cols="80" required>
               {{$editInfo1->info1_about_us}}
            </textarea>
 
               </div>

           

{{--               
               <!-- <div class="form-group">
            <img  id="output" src="{{asset('').$editInfo1->info1_image}}" width="25%"/>

    </div>
    
<div class="form-group">
    <label for="exampleInputFile" class="bmd-label-floating">
    <p  class="btn btn-success mt-4">Add image 1</p>
    </label>
    <input type="file" class="form-control-file" name="info1_image"  id="exampleInputFile" onchange="loadFile(event)"  />
</div>          --> --}}
               
             

<br>


       <div class="text-center">
        <button type="submit"  id="submit-all" class="btn btn-success mt-4">Save</button>

 
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>

@endsection
@push('js')
                     <script>
                         CKEDITOR.replace( 'editor' );
                   CKEDITOR.replace( 'editor1' );
                   </script>

<script>
 var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
};
    
           
              
    </script>
    @endpush