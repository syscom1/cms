@extends('layouts.app', ['activePage' => 'listOur', 'titlePage' => __('Our Work List')])

@section('content')

    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col">
                      <div class="shadow p-3 mb-5 bg-white rounded">  
                        <div class="card-header">
                            <div class="row align-items-center">
                               
                                <div class="col-sm-8">
                                    <h3 class="mb-0">{{ __('List of Our Works') }}</h3>

                                </div>
                                <div class="col-sm-4">
                                <div class="col-md-4 offset-md-4 clearfix">
                                    <a href="{{ route('ourwork.addOur') }}" class="btn btn-primary btn-lg " role="button">{{ __('Add Our Work') }}</a>
                                </div>
                                </div>
                            </div>
                            @if ( Session::get('err') )
                            <div class="alert alert-success alert-dismissible fade show" style="direction: ltr" role="alert">
                                {{ Session::get('err') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                  </div>            
                            @endif

                        </div>
                        
                        <div class="col-12">
                            
                        @if (count($errors) > 0)
                        @if($errors->any())
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{$errors->first()}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                    @endif
                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                        </div>

                 
                        <div class="table-responsive">
                            <table class="table align-items-center table-flush datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">{{ __('#') }}</th>
                                        <th scope="col">{{ __('TITLE') }}</th>
                                        <th scope="col">{{ __('CAPTION') }}</th>
                                        <th scope="col">{{ __('TAGS') }}</th>
                                        <th scope="col">{{ __('Action') }}</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($our as $key=>$user)
                                        <tr>
                                            <td>{{ $key+1}} </td>
                                            <td>{{ $user->our_title}} </td>
                                          
                                            <td>{{ $user->our_caption}} </td>
                                            <td>
                                                    {{ implode(', ', json_decode($user->our_tags,true))}}
                                            </td>
                                            <td >
                                            <a href="deleteOur/{{$user->our_id}}" 
                                               onclick="return confirm('Are you sure you want to delete this Menu?')"   class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                                <i class="material-icons">delete</i></a>

                                            <a href="editOur/{{$user->our_id}}" class="btn btn-success btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                             <i class="material-icons">edit</i></a>

                                             <a href="addImg/{{ $user->our_id }}" class="btn btn-success btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                                <i class="material-icons">image</i></a>
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                       
            
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('js')
    <script>
$('.datatable').DataTable();

    </script>
    @endpush 