@extends('layouts.app', ['activePage' => 'listOur', 'titlePage' => __('Menu Adding')])


@section('content')

<div class="content">
    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
@endif
@endif

    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
</br>


    <div class="container-fluid">
        <div class="card">
        <div class="card-header card-header-text card-header-success text-right">
        <div class="card-text ">
              <h4 class="card-title">Add New Our Work</h4>
            </div>
            </div>
            <div class="card-body">

            <div class="col-md-12">
                <form method="post" action="{{ route('ourwork.addNewOur') }}" autocomplete="off"  enctype="multipart/form-data">
                    @csrf
                    <div class="pl-lg-4">
                        <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Title</label>
                            <br>
                            <input type="text" name="our_title" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('title') }}" autofocus>

                        </div>

                      

                        <div class="form-group{{ $errors->has('caption') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Caption</label>
                            <br>
                            <input type="text" name="our_caption" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('position') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('caption') }}" autofocus>

                        </div>
                        <label for="category_id">Tags</label>
                        <select class="form-control select3" name="our_tags[]" id="category_id" multiple="multiple"  required>
                              
                            </select>
                        
                       

                        <div class="text-center">
                            <a href="{{ route('ourwork.listOur') }}"  class="btn btn-primary mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>

@endsection
 