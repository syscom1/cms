@extends('layouts.app', [
'class' => '',
'activePage' => 'listUser', 'titlePage' => __('Edit User')])

@section('content')
<div class="content">

@if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
</br>

    <div class="container-fluid">
    <div class="card-header">

        <div class="row">

            <div class="col-md-12">
                <form method="post" action="{{ route('user.updateUser') }}" autocomplete="off"  enctype="multipart/form-data">
                    @csrf
                    <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" id="input-id" type="text"   value="{{ $editUser->id }}" hidden/>
                    <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="address_id" id="address_id" type="text"   value="{{ old('address_id', isset($addressUser->address_id))? $addressUser->address_id : NULL  }}" hidden/>
                    <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="type" id="type" type="text"   value="{{ old('type', $editUser->type) }}" hidden/>
                    <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="oldpassword" id="oldpassword" type="text"   value="{{ old('password', $editUser->password) }}" hidden/>

                    <div class="pl-lg-4">
                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Full Name </label>
                            <input type="text" name="name" id="input-name"
                                class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('Enter User Name') }}" value="{{$editUser->name}}" required >

                        </div>


                          <div class="form-group" id="username1">
                            <label class="form-control-label" for="input-name">Username </label>
                            <input type="text" name="username" id="username"
                                class="form-control form-control-alternative"
                                placeholder="{{ __('Enter Username') }}" value="{{$editUser->username}}" required >

                        </div>
                        
                        <div class="form-group">
                            <label class="form-control-label" for="input-name">Email </label>
                            <input type="email" name="email" id="email"
                                class="form-control form-control-alternative"
                                placeholder="{{ __('Enter Email') }}" value="{{$editUser->email}}"  >

                        </div>


                        <div class="form-group">
                            <label class="form-control-label" for="input-name">Password </label>
                            <input type="text" name="password" id="input-name"
                                class="form-control form-control-alternative"
                                placeholder="{{ __('Enter Password') }}" value=""  >

                        </div>
                   
                   
                    <div id="all">
                   
                        <div class="form-group" >
                            <label class="form-control-label" for="input-name">Phone Number</label>
                            <input type="number" name="phone" id="input-name"
                                class="form-control text-right form-control-alternative"
                                placeholder="{{ __('Enter Phone Number') }}" value="{{isset($editUser->phone) ?$editUser->phone : NULL}}" required >

                        </div>
                        
                        {{-- <div class="form-group" >
                            <label class="form-control-label" for="input-name">City</label>
                            <input type="text" name="address_city" id="input-name"
                                class="form-control text-right -control-alternative"
                                placeholder="{{ __('Enter City') }}" value="{{isset($addressUser->address_city) ?$addressUser->address_city : NULL}}" required >

                        </div> --}}
                        
                        {{-- <div class="form-group" >
                            <label class="form-control-label" for="input-name">Street</label>
                            <input type="text" name="address_street" id="input-name"
                                class="form-control text-right form-control-alternative"
                                placeholder="{{ __('Enter Street') }}" value="{{isset($addressUser->address_street) ?$addressUser->address_street : NULL}}" required >

                        </div> --}}
                        


                        {{-- <div class="form-group" >
                            <label class="form-control-label" for="input-name">Building</label>
                            <input type="text" name="address_building" id="input-name"
                                class="form-control text-right form-control-alternative"
                                placeholder="{{ __('Enter Building') }}" value="{{isset($addressUser->address_building) ?$addressUser->address_building : NULL}}" required >

                        </div>
                        
                        <div class="form-group" >
                            <label class="form-control-label" for="input-name">Floor</label>
                            <input type="text" name="address_floor" id="input-name"
                                class="form-control text-right  form-control-alternative"
                                placeholder="{{ __('Enter Street') }}" value="{{isset($addressUser->address_floor) ?$addressUser->address_floor : NULL}}" required >

                        </div>
                        
                        <div class="form-group" >
                            <label class="form-control-label" for="input-name">Apartment Number</label>
                            <input type="text" name="address_apartment_number" id="input-name"
                                class="form-control text-right form-control-alternative"
                                placeholder="{{ __('Enter Street') }}" value="{{isset($addressUser->address_apartment_number) ?$addressUser->address_apartment_number : NULL}}" required >

                        </div>
                        
                            <div class="form-group" >
                            <label class="form-control-label" for="input-name">Special Instruction</label>
                            <input type="text" name="address_special_instruction" id="input-name"
                                class="form-control  text-right form-control-alternative"
                                placeholder="{{ __('Enter Street') }}" value="{{isset($addressUser->address_special_instruction) ?$addressUser->address_special_instruction : NULL}}" required >

                        </div> --}}

                        
                                 <div class="form-group">
                                <label for="user_email" class="form-control-label">Map Location</label>
                            
                                <div id="map" ></div>
                              
                                  </div>
                                  <div class="form-row">
                                  <label   class="form-control-label">Map Coordinates</label>
                                  <div class="col">
                                    <input type="text" id="address_latitude" value="{{isset($addressUser->address_latitude) ? $addressUser->address_latitude : NULL}}" name="address_latitude"   class="form-control form-control-alternative"  disabled  >
                                  
                                  </div>
                                     <div class="col">
                                    <input type="text" id="address_longitute" value="{{isset($addressUser->address_longitute) ? $addressUser->address_longitute : NULL}}" name="address_longitute"   class="form-control form-control-alternative" disabled  >
                                   
                                  </div>
                                </div>


                        </div>



                        <div class="text-center">
                            <a href="{{ route('user.listUser') }}"  class="btn btn-warning mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>
@endsection

@push('js')
    <script>


 if($('#type').val() == '2'){
     
      $('#all :input').each(function(){
           
           $(this).attr('required',true);
           
       })
    // $('#address_special_instruction1').hide();
    // $('#address_apartment_number1').hide();
    // $('#address_floor1').hide();
    // $('#address_building1').hide();
    // $('#address_street1').hide();
    // $('#address_city1').hide();
    // $('#username1').hide();
    $('#all').show();
    
 }
 else{
      $('#all :input').each(function(){
           
           $(this).attr('required',false);
           
       })
    // $('#address_special_instruction1').show();
    // $('#address_apartment_number1').show();
    // $('#address_floor1').show();
    // $('#address_building1').show();
    // $('#address_street1').show();
    // $('#address_city1').show();
    // $('#username1').show();
      $('#all').hide();
 }

if($('#type').val()=='1'){
    
            $("#username").attr('required',false);
            
            $('#username1').hide();

            $("#email").attr('required',true);
            
            


}   

else{
        $("#username").attr('required',true);

        $('#username1').show();

        $("#email").attr('required',false);

          
}

	
	
	var markerPosition ={lat:'',lng:''};
    var map;
	var markers = [];
 


    var lebanon = {lat:33.8547, lng: 35.8623};
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: lebanon,
          zoom: 10
        });
	google.maps.event.addListener(map, 'click', function(event) {
	if(markers.length <1) 
          addMarker(event.latLng, map);
        });
		
	//Edit Map 
	
	var x=document.getElementById("address_latitude").value ;
	var y=document.getElementById("address_longitute").value ;
 
	if(x>0){
	var siteLatLng = new google.maps.LatLng(x, y);
     addMarker(siteLatLng,map);
	 }
	 
     }
 initMap();
    function addMarker(location, map) {


	markerPosition.lat=location.lat();
	    markerPosition.lng=location.lng();
		console.log(markerPosition);
		
	  document.getElementById("address_latitude").value = markerPosition.lat;
	  document.getElementById("address_longitute").value= markerPosition.lng;
      
		//var variableToSend = markerPosition;
        //$.post('offer-add.php', {variable: variableToSend});
        var marker = new google.maps.Marker({
         position: location ,
         map: map
        });
   markers.push(marker);
   marker.addListener("click", function() {
			deleteMarkers(marker);
		
        });
      }
	  
	  
	  
	  
 function deleteMarkers(marker) {
       marker.setMap(null);
 
        markers = [];
	document.getElementById("address_latitude").value = '';
	   document.getElementById("address_longitute").value= '';
      }
      
      
      
      function selectLocation(){
          var lat=document.getElementById("address_latitude").value
          var lng=document.getElementById("address_longitute").value
 
          
      var siteLatLng = new google.maps.LatLng(lat, lng);
       map = new google.maps.Map(document.getElementById('map'), {
          center: lebanon,
          zoom: 10
        });
     addMarker(siteLatLng,map);
         
          
      }
         
    </script>
    @endpush
