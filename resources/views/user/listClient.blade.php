@extends('layouts.app', ['activePage' => 'listClient', 'titlePage' => __('Clients List')])

@section('content')

</br>
</br>
</br>
 
    <div class="container-fluid">
        <div class="card">
        <div class="card-header card-header-text card-header-success text-left">
        <div class="card-text ">
              <h4 class="card-title">Clients</h4>
            </div>
            </div>
            <div class="card-body">

            <div class="col-md-12">
              
              
              
                            <table class="table align-items-center table-flush datatable">
                                <thead class="thead-light">
        <tr>
            <th >#</th>
            <th >Name</th>
            <th >Phone</th>
            <!--<th >View Details</th>-->
            <th >View Orders</th>
            <th >View Details</th>
            

        </tr>
    </thead>
    
    <tbody>
        
        
         @foreach ($listClient as $key=>$user)
                                        <tr>
                                            <td>{{ $key+1}} </td>
                                            <td>{{ $user->name}} </td>
                                            <td>{{ $user->phone}}</td>
                                            
                                     <!--       <td>-->
                                     <!--  <button id="showDetails" class="btn btn-info btn-fab btn-fab-mini btn-round infoU" data-toggle="modal"-->
                                     <!--data-id="{{ $user->id }}"  data-name="{{ $user->name }}" -->
                                     <!--data-phone="{{ $user->phone }}" data-username="{{ $user->username }}" data-password="{{ $user->password }}" data-status="{{ $user->status }}"-->
                                     <!--  data-target="#detailsModal"  aria-disabled="true">-->
                                     <!--           <i class="material-icons">list_alt</i></button>-->
                                     <!--      </td>-->
                                           
                                            <td>
                                           
                                                <a href="{{ route('user.listUserOrders',['id' =>  $user->id]) }}"
                                                 class="btn btn-primary btn-fab btn-fab-mini btn-round"  role="button" aria-disabled="true">
                                                <i class="material-icons">remove_red_eye</i></a>
                                           
                                            </td>                                           
                                            
                                            
                                         <td>
                                           
                                                <a href="{{ route('user.listUserAddress',['id' =>  $user->id]) }}"
                                                 class="btn btn-success btn-fab btn-fab-mini btn-round"   role="button" aria-disabled="true">
                                                <i class="material-icons">location_on</i></a>
                                           
                                            </td>  
                                            
                                        </tr>
                                    @endforeach
        
    </tbody>
</table>
              
              
              
              
              
            </div>
            </div>
        </div>
    </div>
    </br>
   <!--MODAL FOR DETAILS-->
 
    <div class="modal fade" id="detailsModal" tabindex="-1" role="">
    <div class="modal-dialog modal-login modal-lg" role="document">
        <div class="modal-content ">
           
 
                <div class="modal-body ">
                    <form class="form" >
                         <div class="card-body">
                             
                            <table class="table align-items-center table-flush ">
                                <thead class="thead-light">
        <tr>
            <th >User ID</th>
            <th >Name</th>
            <th >Username</th>
            <th>Password</th>
            <th >Phone</th>
            <th>Status</th>
        </tr>
    </thead>
    
    <tbody>
        
        
        
                                        <tr>
                                            <td>
                                                <p id="idModal"></p> 
                                            </td>
                                                
                                            <td> 
                                            <p id="nameModal"></p> 
                                            </td>
                                            
                                            <td>
                                                <p id="usernameModal"></p> 
                                            </td>
                                              <td>
                                                <p id="passwordModal"></p> 
                                            </td>
                                            <td>
                                                <p id="phoneModal"></p> 
                                            </td>
                                            
                                            <td>
                                               <p id="statusModal"></p> 
                                            
                                           </td>
                                           
                                        
                                        </tr>
                               
        
    </tbody>
</table>
</div>
 </form>
</div>
  </div>
  </div>
   </div>
    


<!--MODAL FOR ORDERS-->

<!--    <div class="modal fade" id="ordersModal" tabindex="-1" role="">-->
<!--    <div class="modal-dialog modal-login modal-lg" role="document">-->
<!--        <div class="modal-content ">-->
           
 
<!--                <div class="modal-body ">-->
<!--                    <form class="form" >-->
<!--                         <div class="card-body">-->
                             
<!--                    <table class="table-responsive datatable" id="datatable1">-->
<!--    <thead>-->
<!--        <tr>-->
<!--            <th >#</th>-->
<!--            <th >Name</th>-->
<!--            <th >Username</th>-->
<!--            <th >Phone</th>-->
<!--            <th>Status</th>-->
<!--        </tr>-->
<!--    </thead>-->
    
<!--    <tbody>-->
        
        
<!--         @foreach ($listClient as $key=>$user)-->
<!--                                        <tr>-->
<!--                                            <td>{{ $key+1}} </td>-->
<!--                                            <td>{{ $user->name}} </td>-->
<!--                                            <td>{{ $user->username}} </td>-->
<!--                                            <td>{{ $user->phone}}</td>-->
<!--                                            <td>-->
                                                
<!--                                                 @if($user->status=='1')-->
<!--                                                 <a href="#" class="btn btn-success btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">-->
<!--                                                <i class="material-icons">check</i></a>-->
<!--                                                @endif -->
                                                
<!--                                                @if($user->status=='0')-->
<!--                                                <a href="#" class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">-->
<!--                                                <i class="material-icons">close</i></a>-->
<!--                                                @endif -->
<!--                                                </td>-->
                                           
                                        
<!--                                        </tr>-->
<!--                                    @endforeach-->
        
<!--    </tbody>-->
<!--</table>-->
<!--</div>-->
<!-- </form>-->
<!--</div>-->
<!--  </div>-->
<!--  </div>-->
<!--   </div>-->


@endsection


@push('js')
    <script>
    $('.datatable').DataTable();




$(document).ready(function () {

$(".infoU").click(function (e) {
    $("#idModal").text($(this).attr('data-id'));
    $("#nameModal").text($(this).attr('data-name'));
    $("#usernameModal").text($(this).attr('data-username'));
        $("#passwordModal").text($(this).attr('data-password'));

    $("#phoneModal").text($(this).attr('data-phone'));


       if( $(this).attr('data-status') == 1){
            $("#statusModal").text("Active");
}

else{
                $("#statusModal").text("Inactive");

}
});
});


// $(document).ready(function () {

//             $(".infoU").click(function (e) {
//                 $currID = $(this).attr("data-id");
//                 $.post("listClient.blade.php", {id: $currID}, function (data) {
//                     $('#idModal').html(data);
//                     }
//                 );
//             });
//         });


// $('#infoU').on('show', function(e) {
//     var link     = e.relatedTarget(),
//         modal    = $(this),
//         username = link.data("username"),
//         name    = link.data("name");

//   //  modal.find("#email").val(email);
//   $("#idModal").text(name);
// });

    </script>
    @endpush