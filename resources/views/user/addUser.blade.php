@extends('layouts.app', ['activePage' => 'listUser', 'titlePage' => __('Add User')])


@section('content')
 
<div class="content">
    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
</br>


    <div class="container-fluid">
    <div class="card">
        <div class="card-header card-header-text card-header-success text-right">
        <div class="card-text ">
              <h4 class="card-title">Add New User</h4>
            </div>
            </div>
            <div class="card-body">              
            <div class="col-md-12">
                <form method="post" action="{{ route('user.addNewUser') }}" autocomplete="off"  enctype="multipart/form-data" >
                    @csrf
                    <div class="pl-lg-4">

                    <div class="form-group">
                            <label for="exampleFormControlSelect1">Select User Type</label>
                            <select class="form-control select2" width="100%" id="typeS" name="typeS1" required>
                                <option value="">Select</option>
                                <option value="1">Admin</option>
                                <!--<option value="3">Delivery Boy</option>-->
                            </select>
                        </div>


                        <div class="form-group">
                            <label class="form-control-label" for="input-name">Full Name </label>
                            <br>
                            <input type="text" name="name" id="name"
                                class="form-control form-control-alternative"
                                placeholder="{{ __('Enter Here Full Name ') }}"  required >

                        </div>
                            <div class="form-group" id="email1">
                            <label class="form-control-label" for="input-name" id="labelemail">Email Address</label>
                            <br>
                            <input type="email" name="email" id="email"
                                class="form-control  form-control-alternative"
                                placeholder="{{ __('Enter Here Email Address ') }}"   >

                        </div>
                        
                          <div class="form-group"  id="username1">
                            <label class="form-control-label" for="input-name">Username</label>
                            <br>
                            <input type="text" name="username" id="username"
                                class="form-control form-control-alternative"
                                placeholder="{{ __('Enter Here Username ') }}"   required>

                        </div>
                        
                        
                        <div class="form-group">
                            <label class="form-control-label" for="input-name">Password </label>
                            <br>
                            <input type="text" name="password" id="password"
                                class="form-control form-control-alternative"
                               placeholder="{{ __('Enter Here password ') }}" required >  <!-- required -->

                        </div>
                        
                        

                        <div class="form-group" id="phone1">
                            <label class="form-control-label" for="input-name">Mobile Number </label>
                            <br>
                            <input type="text" name="phone" id="phone"
                                class="form-control form-control-alternative"
                                placeholder="{{ __('Enter Here Mobile Number ') }}"   required>

                        </div>
                      
                 

            

                        <div class="text-center">
                            <a href="{{ route('user.listUser') }}"  class="btn btn-primary mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>

@endsection

@push('js')
<script>


var select = document.getElementById('typeS');

select.onchange = function() {
    console.log($("#typeS option:selected").val())
 if($("#typeS option:selected").val() == '1' || $("#typeS option:selected").val() == '3'){
     
    // $('#address_special_instruction1').hide();
  
       
     
       $("#email").attr('required',true);

    
 }
 else{

  
       $("#email").removeAttr('required');

 }
 
 
 if($("#typeS option:selected").val() == '1'){
     
                      $("#username").attr('required',false);

                      $('#username1').hide();
                    
                      $("#email").attr('required',true);
                      $('#email1').show();
                     


 }
 
 else{
     
                      $("#username").attr('required',true);

                      $('#username1').show();
                    
                      $("#email").attr('required',false); 
                      $('#email1').hide();
 }

}


</script>
@endpush
