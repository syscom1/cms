@extends('layouts.app', ['activePage' => 'listUserOrders', 'titlePage' => __('Client Orders List')])

@section('content')

</br>
</br>
</br>
 
    <div class="container-fluid">
        <div class="card">
        <div class="card-header card-header-text card-header-success text-left">
        <div class="card-text ">
              <h4 class="card-title">All Orders</h4>
            </div>
            </div>
            <div class="card-body">

            <div class="col-md-12">
              
              
     
                            <table class="table align-items-center table-flush datatable">
                                <thead class="thead-light">
        <tr>
            <th >#</th>
            <th >Order Number</th>
            <th >Total Price</th>
            <th >Order Status</th>
         
            

        </tr>
    </thead>
    
    <tbody>
        
        
         @foreach ($listUserOrders as $key=>$user)
                                        <tr>
                                            <td>{{ $key+1}} </td>
 <td>
                                             <a href="listDetail/{{ $user->order_id}}" 
                                            class="btn btn-info btn-round" role="button" aria-disabled="true">
                                             {{ $user->order_number}}
                                             </a>
                                               </td>
                                               
                                            <td>{{ $user->order_totalprice}}</td>
                                            
<td>
                                                @if($user->order_status == 'P')
                                                Pending
                                                @elseif ($user->order_status == 'S')
                                                Shipped
                                                @elseif ($user->order_status == 'D')
                                                Done
                                                @elseif ($user->order_status == 'R')
                                                Rejected
                                                @endif
                                                
                                                
                                                </td>                                                                                
                                            
                                            
                                        
                                        </tr>
                                    @endforeach
        
    </tbody>
</table>
              
              
              
              
              
            </div>
            </div>
        </div>
    </div>
    <div class="text-center">
                            <a href="{{ route('user.listClient') }}"  class="btn btn-warning mt-4">{{ __('Back') }}</a>

                        </div>
@endsection


@push('js')
    <script>
        $('.datatable').DataTable();

      </script>
    @endpush