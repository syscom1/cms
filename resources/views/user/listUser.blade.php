@extends('layouts.app', ['activePage' => 'listUser', 'titlePage' => __('Users Admins')])


@section('content')
</br>
</br>
</br>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <h3 class="mb-0">{{ __('Admins') }}</h3>
                                </div>
                                <div class="col-sm-4">
                                <div class="col-md-4 offset-md-4 clearfix">
                                    <a href="{{ route('user.addUser') }}" class="btn btn-primary btn-lg " role="button">{{ __('Add Admin') }}</a>
                                </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            
        @if (count($errors) > 0)
        @if($errors->any())
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{$errors->first()}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif
    @endif
                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                        </div>



<!--                        <form enctype="multipart/form-data">-->
<!--                        <div class="form-row">-->
<!--                        <div class="form-group col-sm-2" style="align-self: center ;">-->

<!--<select class="js-example-basic-multiple js-states form-control select2"  name="type" >-->
<!--                        <option value="">Select Type</option>-->
<!--                        <option         @if(isset($_REQUEST['type']) and !empty($_REQUEST['type']))-->
<!--                            @if($_REQUEST['type'] == '1' )-->

<!--                                selected  -->
<!--                               @endif-->
<!--                                @endif-->
<!--                                 value="1">Admin</option>-->
<!--                        <option   @if(isset($_REQUEST['type']) and !empty($_REQUEST['type']))-->
<!--                            @if($_REQUEST['type'] == '2' )-->

<!--                                selected  -->
<!--                               @endif-->
<!--                                @endif value="2">Client</option>-->
<!--                                <option   @if(isset($_REQUEST['type']) and !empty($_REQUEST['type']))-->
<!--                            @if($_REQUEST['type'] == '3' )-->

<!--                                selected  -->
<!--                               @endif-->
<!--                                @endif value="3">Delivery Boy</option>-->

<!--                        </select>-->
<!--                        </div>-->

<!--                        <div class="form-group col-sm-2" style="align-self: center ;">-->

<!--                        <input type="text" name="search" id="searchVal"-->
<!--                                class="form-control form-control-alternative"-->
<!--                                placeholder="Search" value="{{isset( $_REQUEST['search'] ) ?  $_REQUEST['search']  : ''}}">-->
<!--                                                        </div>-->


<!--                        <div class="form-group col-sm-2">-->
<!--                        <button type="submit" class="btn btn-success btn-fab btn-fab-mini">-->
<!--                                            <i class="material-icons">search</i>-->
<!--                                                </button> -->


<!--                                                <a href="{{route('user.listUser')}}" class="btn btn-danger btn-fab btn-fab-mini">-->
<!--                      <i class="material-icons">close</i>-->
<!--</a>-->
<!--                        </div>-->

<!--                        </div>-->
<!--                        </form>-->


                        <div class="table-responsive">
                            <table class="table align-items-center table-flush datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">{{ __('#') }}</th>
                                        <th scope="col">{{ __('Name') }}</th>
                                        <th scope="col">{{ __('Type') }}</th>
                                        <th scope="col">{{ __('Action') }}</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($listUser as $key=>$user)
                                        <tr>
                                            <td>{{ $key+1}} </td>
                                            <td>{{ $user->name}} </td>
                                            <td>
                                            
                                                @if($user->type=='1')
                                                <button class="btn btn-danger btn-sm">Admin</button>
                                                @endif                                            
                                                @if($user->type=='2')
                                                <button class="btn btn-success btn-sm">Client</button>
                                                @endif   
                                                @if($user->type=='3')
                                                <button class="btn btn-warning btn-sm">Delivery Boy</button>
                                                @endif 

                                            </td>
                                            <td >
                                               
                                               @if($user->status == 0)
                                               <a href="statusUser/{{$user->id}}/1"  class="btn btn-primary btn-fab btn-fab-mini btn-round"
                                               onclick="return confirm('Are you sure you want to restore this User?')"  role="button" aria-disabled="true">
                                                <i class="material-icons">refresh</i></a>
                                                   @endif
   
                                                   @if($user->status == 1)
   
                                                <a href="statusUser/{{$user->id}}}/0" 
                                               onclick="return confirm('Are you sure you want to hide this User?')"   class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                                <i class="material-icons">close</i></a>
                                                @endif
   
                                                <a href="editUser/{{$user->id}}" class="btn btn-success btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                                <i class="material-icons">edit</i></a>
       
                                               </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>



                       
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('js')
    <script>
$('.datatable').DataTable();

    </script>
    @endpush