@extends('layouts.app', ['activePage' => 'listClient', 'titlePage' => __('Client Details')])

@section('content')

</br>
</br>
</br>
 
    <div class="container-fluid">
        <div class="card">
        <div class="card-header card-header-text card-header-primary text-left">
        <!--<div class="card-text ">-->
        <!--    </div>-->
            
            
 <div class="card col-sm-12 ">
              <div class="card-header card-header-info">
                <h4 class="card-title"> Details</h4>
                <!-- <p class="card-category">{{ __('User information') }}</p> -->
              </div>
              <div class="card-body ">

              <div class="row">
              <h4 class="col-sm-12 col-form-label">Client: {{$listUserData->name}}</h4>
              <h4 class="col-sm-12 col-form-label">Username: {{$listUserData->username}}</h4>
              <h4 class="col-sm-12 col-form-label">Password: {{$listUserData->password}}</h4>

              <h4 class="col-sm-12 col-form-label">Phone: {{$listUserData->phone}}</h4>
              <h4 class="col-sm-12 col-form-label">Status: @if($listUserData->status == 1)
                                                              Active
                                                              @else
                                                              Inactive
                                                              @endif</h4>

              </div>

                <div class="row">
                <div class="col">
                </div>

                <div class="col-md-3 offset-2">


                </div>
                </div>

               

            </div>
            
  </div>
             
            
            
            
            </div>
            <div class="card-body">

            <div class="col-md-12">
              
              
   
                            <table class="table align-items-center table-flush datatable">
                                <thead class="thead-light">
        <tr>
            <th >#</th>
            <th >Address city</th>
            <th >Address Street</th>
            <th >Address Building</th>
            <th >Address Floor</th>
            <th >Address Apartment #</th>
            <th >Address Special Instruction</th>
         
            

        </tr>
    </thead>
    
    <tbody>
        
        
         @foreach ($listUserAddress as $key=>$user)
                                        <tr>
                                            <td>{{ $key+1}} </td>
                                            <td>{{ $user->city_name}} </td>
                                            <td>{{ $user->address_street}}</td>
                                            <td>{{ $user->address_building}}</td>
                                            <td>{{ $user->address_floor}}</td>
                                            <td>{{ $user->address_apartment_number}}</td>
                                            <td>{{ $user->address_special_instruction}}</td>
                                                                                
                                            
                                            
                                        
                                        </tr>
                                    @endforeach
        
    </tbody>
</table>
              
              
              
              
              
            </div>
            </div>
        </div>
    </div>
    <div class="text-center">
                            <a href="{{ route('user.listClient') }}"  class="btn btn-warning mt-4">{{ __('Back') }}</a>

                        </div>
@endsection


@push('js')
    <script>
        $('.datatable').DataTable();

      </script>
    @endpush