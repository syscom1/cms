@extends('layouts.app', ['activePage' => 'addContact', 'titlePage' => __('Contact Adding')])


@section('content')

<div class="content">
@if($s == '1')
    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
@endif

@if($s == '0')
    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
@endif
</br>


    <div class="container-fluid">
        <div class="card">
        <div class="card-header card-header-text card-header-success text-right">
        <div class="card-text ">
              <h4 class="card-title">Add New Contact</h4>
            </div>
            </div>
            <div class="card-body">

            <div class="col-md-12">
                <form method="post" action="{{ route('contact.addNewContact') }}" autocomplete="off"  enctype="multipart/form-data">
                    @csrf
                    <div class="pl-lg-4">
                        <div class="form-group{{ $errors->has('contact_address') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Address</label>
                            <br>
                            <input type="text" name="contact_address" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('contact_address') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('contact_address') }}" autofocus>

                        </div>
                        

                        
                        <div class="form-group{{ $errors->has('contact_phone') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Phone </label>
                            <br>
                            <input type="text" name="contact_phone" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('contact_phone') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('contact_phone') }}" required autofocus>

                        </div>

             
                        <div class="form-group{{ $errors->has('contact_email') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Email </label>
                            <br>
                            <input type="text" name="contact_email" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('contact_email') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('contact_email') }}" required autofocus>

                        </div>

                        <div class="form-group{{ $errors->has('contact_facebook') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Facebook </label>
                            <br>
                            <input type="text" name="contact_facebook" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('contact_facebook') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('contact_facebook') }}" autofocus>

                        </div>

                        <div class="form-group{{ $errors->has('contact_instagram') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Instagram </label>
                            <br>
                            <input type="text" name="contact_instagram" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('contact_instagram') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('contact_instagram') }}" autofocus>

                        </div> 

                        <div class="form-group{{ $errors->has('contact_twitter') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Twitter </label>
                            <br>
                            <input type="text" name="contact_twitter" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('contact_twitter') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('contact_twitter') }}" autofocus>

                        </div>

                        <div class="form-group{{ $errors->has('fax') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Fax </label>
                            <br>
                            <input type="text" name="fax" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('fax') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('fax') }}" autofocus>

                        </div> 
                        

                        <div class="form-group">
 
             <label class="form-control-label" for="input-name" >Image </label><br>
             <img id="output" width="50%"/>
                         </div>
                           <div class="form-group">
 
     <label for="exampleInputFile" class="bmd-label-floating">
                                     <p  class="btn btn-success mt-4">
         Click to add image</p>
 </label>
     <input type="file" class="form-control-file" name="image"  id="exampleInputFile" onchange="loadFile(event)"  />
    </div> 
                                        
                        
                        <div class="text-center">
                            <a href="{{route('contact.listContact')}}"  class="btn btn-primary mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>


@push('js')
<script>
    var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
      }
    };
  </script>
      @endpush
@endsection
