@extends('layouts.app', ['activePage' => 'listContact', 'titlePage' => __('Contact List')])

@section('content')

    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="card-header">
                            <div class="row align-items-center">
                               
                                <div class="col-sm-8">
                                    <h3 class="mb-0">{{ __('Contact') }}</h3>

                                </div>


                                <div class="col-sm-4">
                                <div class="col-md-4 offset-md-4 clearfix">
                                    <a href="{{ route('contact.addContact') }}" class="btn btn-primary btn-lg " role="button">{{ __('Add  Contact') }}</a>
                                </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            
        @if (count($errors) > 0)
        @if($errors->any())
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{$errors->first()}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif
    @endif
                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                        </div>

                        <div class="table-responsive">
                            <table class="table align-items-center table-flush datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">{{ __('#') }}</th>
                                        <th scope="col">{{ __('Address') }}</th>
                                        <th scope="col">{{ __('Phone') }}</th>
                                        <th scope="col">{{ __('Email') }}</th>
                                        <th scope="col">{{ __('Facebook') }}</th>
                                        <th scope="col">{{ __('Instagram') }}</th>
                                        <th scope="col">{{ __('Twitter') }}</th>
                                        <th scope="col">{{ __('Fax') }}</th>
                                        <th scope="col">{{ __('Top Image') }}</th>
                                        <th scope="col">{{ __('Action') }}</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($listContact as $key=>$user)
                                        <tr>
                                            <td>{{ $key+1}} </td>
                                            <td>{{ $user->contact_address}} </td>
                                            <td>{{ $user->contact_phone}} </td>
                                            <td>{{ $user->contact_email}} </td>
                                            <td>{{ $user->contact_facebook}} </td>
                                            <td>{{ $user->contact_instagram}} </td>
                                            <td>{{ $user->contact_twitter}} </td>
                                            <td>{{ $user->fax}} </td>
                                            <td>
                                                @if ($user->image)
                                                <button id="click1{{ $user->id }}" class="btn btn-primary">View</button>
                                                @push('js')
                                                <script>
                                                   $('#click1{{ $user->id }}').on('click',function(){
                                                       swal.fire({
                                                        imageUrl: '{{ asset($user->image) }}',
                                                        imageHeight: 200,
                                                        imageAlt: 'A tall image'
                                                       })
                                                   })
                                                </script>
                                            @endpush
                                            @endif
                                            </td>
                                            
                                            <td >
                                            <a href="deleteContact/{{$user->contact_id}}" 
                                               onclick="return confirm('Are you sure you want to delete this Contact?')"   class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                                <i class="material-icons">delete</i></a>

                                            @if($user->contact_status == 0)
                                            <a href="statusContact/{{$user->contact_id}}/1"  class="btn btn-primary btn-fab btn-fab-mini btn-round"
                                            onclick="return confirm('Are you sure you want to restore this Contact?')" class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                             <i class="material-icons">refresh</i></a>
                                                @endif
                                                @if($user->contact_status == 1)

                                             <a href="statusContact/{{$user->contact_id}}/0" 
                                            onclick="return confirm('Are you sure you want to hide this Contact?')"  class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                             <i class="material-icons">close</i></a>
                                             @endif

                                            <a href="editContact/{{$user->contact_id}}" class="btn btn-success btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                             <i class="material-icons">edit</i></a>


                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>



                       
            
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('js')
    <script>
$('.datatable').DataTable();

    </script>
    @endpush 