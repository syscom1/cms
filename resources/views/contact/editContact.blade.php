@extends('layouts.app', ['activePage' => 'editContact', 'titlePage' => __('Contact Editing')])

@section('content')
<div class="content">

@if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
</br>

    <div class="container-fluid">
    <div class="card-header">

        <div class="row">

            <div class="col-md-12">
                <form method="post" action="{{ route('contact.updateContact') }}" autocomplete="off"  enctype="multipart/form-data">
                    @csrf
                    <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="contact_id" id="input-id" type="text"   value="{{ old('id', $editContact->contact_id) }}" hidden/>

                   
                     
                    <div class="pl-lg-4">
                        <div class="form-group{{ $errors->has('contact_address') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Address</label>
                            <input type="text" name="contact_address" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('contact_address') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('') }}" value="{{$editContact->contact_address}}" autofocus>

                        </div>


                        {{-- <div class="form-group{{ $errors->has('contact_address_fr') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Address FR</label>
                            <input type="text" name="contact_address_fr" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('contact_address_fr') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('') }}" value="{{$editContact->contact_address_fr}}" required autofocus>

                        </div> --}}




                    
                        <div class="form-group{{ $errors->has('contact_phone') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Phone</label>
                            <input type="text" name="contact_phone" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('contact_phone') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('') }}" value="{{$editContact->contact_phone}}" autofocus>

                        </div>


              

                      
                        <div class="form-group{{ $errors->has('contact_email') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Email</label>
                            <input type="text" name="contact_email" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('contact_email') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('') }}" value="{{$editContact->contact_email}}" autofocus>

                        </div>

                        <div class="form-group{{ $errors->has('contact_facebook') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Facebook</label>
                            <input type="text" name="contact_facebook" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('contact_facebook') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('') }}" value="{{$editContact->contact_facebook}}" autofocus>

                        </div>


                        <div class="form-group{{ $errors->has('contact_instagram') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Instagram</label>
                            <input type="text" name="contact_instagram" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('contact_instagram') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('') }}" value="{{$editContact->contact_instagram}}" autofocus>

                        </div>

                        <div class="form-group{{ $errors->has('contact_twitter') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Twitter</label>
                            <input type="text" name="contact_twitter" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('contact_twitter') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('') }}" value="{{$editContact->contact_twitter}}" autofocus>

                        </div>
                        
                        <div class="form-group{{ $errors->has('fax') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Fax</label>
                            <input type="text" name="fax" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('fax') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('') }}" value="{{$editContact->fax}}" autofocus>

                        </div>
                        <label class="form-control-label">Image</label><br>
                        <div class="form-group">
                            <img  id="output" src="{{asset('').$editContact->image}}" width="50%"/>
                
                                        </div>
                                          <div class="form-group">
                
                    <label for="exampleInputFile" class="bmd-label-floating">
                                                    <p  class="btn btn-success mt-4">
                        Click to add image</p>
                </label>
                    <input type="file" class="form-control-file" name="image"  id="exampleInputFile" onchange="loadFile(event)"/>
                   </div> 

                        <div class="text-center">
                            <a href="{{ route('contact.listContact') }}"  class="btn btn-warning mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>
@endsection

@push('js')
<script>
    var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
      output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
      }
    };
  </script>
      @endpush