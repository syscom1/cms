@extends('layouts.app', [
'class' => '',
'activePage' => 'edit Services', 'titlePage' => __('Edit Services')])

@section('content')
<div class="content">

@if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
</br>

    <div class="container-fluid">
    <div class="card-header">

        <div class="row">

            <div class="col-md-12">
                <form method="post" action="{{ route('services.updateServices') }}" autocomplete="off"  enctype="multipart/form-data" >
                    @csrf
                    <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="services_id" id="input-id" type="text"   value="{{ old('id', $editServices->id) }}" hidden/>
                    <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="services_picture_old" id="input-id" type="hidden"   value="{{ old('id', $editServices->services_picture) }}" hidden/>


                    <div class="form-group row">
                        <div class="form-check form-check-radio form-check-inline">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="service_design" id="inlineRadio1" value="1"><img src="{{ asset('design/1.png') }}" width="300px" height="150px">
                            <span class="circle">
                                <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check form-check-radio form-check-inline">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="service_design" id="inlineRadio2" value="2"><img src="{{ asset('design/2.png') }}" width="300px" height="150px">
                            <span class="circle">
                                <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check form-check-radio form-check-inline ">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="service_design" id="inlineRadio3" value="3" ><img src="{{ asset('design/3.png') }}" width="300px" height="150px">
                            <span class="circle">
                                <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check form-check-radio form-check-inline ">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="service_design" id="inlineRadio4" value="4" ><img src="{{ asset('design/4.png') }}" width="300px" height="150px">
                            <span class="circle">
                                <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check form-check-radio form-check-inline ">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="service_design" id="inlineRadio5" value="5" ><img src="{{ asset('design/5.png') }}" width="300px" height="150px">
                            <span class="circle">
                                <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check form-check-radio form-check-inline ">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="service_design" id="inlineRadio6" value="6" ><img src="{{ asset('design/6.png') }}" width="300px" height="150px">
                            <span class="circle">
                                <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check form-check-radio form-check-inline ">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="service_design" id="inlineRadio7" value="7" ><img src="{{ asset('design/7.png') }}" width="300px" height="150px">
                            <span class="circle">
                                <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        <div class="form-check form-check-radio form-check-inline ">
                          <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="service_design" id="inlineRadio8" value="8" ><img src="{{ asset('design/8.png') }}" width="300px" height="150px">
                            <span class="circle">
                                <span class="check"></span>
                            </span>
                          </label>
                        </div>
                        </div>

                    <div class="pl-lg-4">
                    
                              <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                              <label class="form-control-label" for="input-name">Title</label>
                              <br>
                              <input type="text" name="services_title" id="input-name"
                                  class="form-control  form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                  placeholder="{{ __(' ') }}" value="{{ $editServices->services_title }}" autofocus>
  
                          </div>

                        <div class="form-group">
             <label style="right: 0;" class="right">Text</label>

     <textarea name="services_text" id="editor" class="editor" rows="10" cols="80" required>
               {{$editServices->services_text}}
            </textarea>
 
               </div>
                  <div class="form-group">
            <img  id="output" src="{{asset('').$editServices->services_picture}}" width="50%"/>

                        </div>
                          <div class="form-group">

    <label for="exampleInputFile" class="bmd-label-floating">
                                    <p  class="btn btn-success mt-4">
        Click to add image</p>
</label>
    <input type="file" class="form-control-file" name="services_picture"  id="exampleInputFile" onchange="loadFile(event)"/>
   </div>
                        

                        <div class="text-center">
                            <a href="{{ route('services.listServices') }}"  class="btn btn-warning mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>
@endsection

@push('js')
<script>
                         CKEDITOR.replace( 'editor' );
                   CKEDITOR.replace( 'editor1' );
           </script>

<script>

var loadFile = function(event) {
    //   $('#output').prop('src', null);

      console.log(event)
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };

    </script>
    @endpush
