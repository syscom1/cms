@extends('layouts.app', ['activePage' => 'listServices', 'titlePage' => __('Services List')])


@section('content')
</br>
</br>
</br>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <h3 class="mb-0">{{ __('Services') }}</h3>
                                </div>
                                <div class="col-sm-4">
                                <div class="col-md-4 offset-md-4 clearfix">
                                    <a href="{{ route('services.addServices') }}" class="btn btn-primary btn-lg " role="button">{{ __('Add  Services') }}</a>
                                </div>
                                </div>
                            </div>
                        </div>
                        @if ( Session::get('err') )
                        <div class="alert alert-success alert-dismissible fade show" style="direction: ltr" role="alert">
                            {{ Session::get('err') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                              </div>            
                        @endif
                        <div class="col-12">
                            
        @if (count($errors) > 0)
        @if($errors->any())
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{$errors->first()}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif
    @endif
                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                        </div>


                        <div class="table-responsive">
                            <table class="table align-items-center table-flush datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">{{ __('#') }}</th>
                                        <th scope="col">{{ __('Title') }}</th>
                                        <th scope="col">{{ __('Background') }}</th>
                                        <th scope="col">{{ __('Design') }}</th>
                                        <th scope="col">{{ __('Action') }}</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($listServices as $key=>$user)
                                        <tr>
                                            <td>{{ $key+1}} </td>
                                            <td>{{ $user->services_title}} </td>
                                            <td><img src="{{ asset($user->services_picture) }}" width="100px" height="100px"></td> 
                                            <td>{{ $user->services_design }} </td>
                                            <td >

                                            <a href="deleteServices/{{$user->id}}" 
                                               onclick="return confirm('Are you sure you want to delete this Services?')"   class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                                <i class="material-icons">delete</i></a>
                                               
                                            @if($user->services_status == 0)
                                            <a href="statusServices/{{$user->id}}/1"  class="btn btn-primary btn-fab btn-fab-mini btn-round"
                                            onclick="return confirm('Are you sure you want to restore this Services?')"  class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                             <i class="material-icons">refresh</i></a>
                                                @endif

                                                @if($user->services_status == 1)

                                             <a href="statusServices/{{$user->id}}/0" 
                                            onclick="return confirm('Are you sure you want to hide this Services?')" class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                             <i class="material-icons">close</i></a>
                                             @endif

                                             <a href="editServices/{{$user->id}}" class="btn btn-success btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                             <i class="material-icons">edit</i></a>
    
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>


                      
               
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('js')
    <script>
$('.datatable').DataTable();

    </script>
    @endpush