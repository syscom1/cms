@extends('layouts.app', ['activePage' => 'ListNews', 'titlePage' => __('News Editing')])

@section('content')
    <div class="content">

        @if (count($errors) > 0)
            @if ($errors->any())
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ $errors->first() }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        @endif
        </br>

        <div class="container-fluid">
            <div class="card-header">

                <div class="row">

                    <div class="col-md-12">
                        <form method="post" action="{{ route('news.updateNews') }}" autocomplete="off"
                            enctype="multipart/form-data">
                            @csrf
                            <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id"
                                id="input-id" type="text" value="{{ old('id', $news->news_id) }}" hidden />



                            <div class="pl-lg-4">
                                <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">Title</label>
                                    <input type="text" name="news_title" id="input-name"
                                        class="form-control  form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                        placeholder="{{ __('') }}" value="{{ $news->news_title }}" required
                                        autofocus>

                                </div>


                                <div class="form-group{{ $errors->has('position') ? ' has-danger' : '' }}">
                                    <label class="form-control-label" for="input-name">Caption</label>
                                    <input type="text" name="news_caption" id="input-name"
                                        class="form-control  form-control-alternative{{ $errors->has('position') ? ' is-invalid' : '' }}"
                                        placeholder="{{ __('') }}" value="{{ $news->news_caption }}" required
                                        autofocus>

                                </div>



                                <img id="output" width="50%" />
                                <div class="form-group">
                                <label class="form-control-label" for="input-name">Image </label>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile" class="bmd-label-floating">
                                        <p class="btn btn-success mt-4">
                                            Click to add image</p>
                                    </label>
                                    <input type="file" class="form-control-file" name="news_image" id="exampleInputFile"
                                        onchange="loadFile(event)" />
                                </div>

                                <div class="text-center">
                                    <a href="{{ route('news.listNews') }}"
                                        class="btn btn-warning mt-4">{{ __('Back') }}</a>

                                    <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </br>
    </div>
@endsection

@push('js')
    <script>

    </script>
@endpush
@push('js')
    <script>
        CKEDITOR.replace('editor');
        CKEDITOR.replace('editor1');
    </script>

    <script>
        var loadFile = function(event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        };
    </script>
@endpush
