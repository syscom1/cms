@extends('layouts.app', ['activePage' => 'addMenu', 'titlePage' => __('Menu Adding')])


@section('content')

<div class="content">
@if($s == '1')
    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
@endif

@if($s == '0')
    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
@endif
</br>


    <div class="container-fluid">
        <div class="card">
        <div class="card-header card-header-text card-header-success text-right">
        <div class="card-text ">
              <h4 class="card-title">Add New Menu</h4>
            </div>
            </div>
            <div class="card-body">

            <div class="col-md-12">
                <form method="post" action="{{ route('menu.addNewMenu') }}" autocomplete="off"  enctype="multipart/form-data">
                    @csrf
                    <div class="pl-lg-4">
                        <div class="form-group{{ $errors->has('title_english') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Title</label>
                            <br>
                            <input type="text" name="title" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('title') }}" required autofocus>

                        </div>

                      

                        <div class="form-group{{ $errors->has('position') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Position </label>
                            <br>
                            <input type="text" name="position" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('position') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('position') }}" required autofocus>

                        </div>
                        
                        <div class="form-group{{ $errors->has('url') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >URL </label>
                            <br>
                            <input type="text" name="url" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('url') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('url') }}" required autofocus>

                        </div>
                        
                        <div class="text-center">
                            <a href="{{ route('menu.listMenu') }}"  class="btn btn-primary mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>

@endsection
