@extends('layouts.app', ['activePage' => 'editMenu', 'titlePage' => __('Menu Editing')])

@section('content')
<div class="content">

@if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
</br>

    <div class="container-fluid">
    <div class="card-header">

        <div class="row">

            <div class="col-md-12">
                <form method="post" action="{{ route('menu.updateMenu') }}" autocomplete="off"  enctype="multipart/form-data">
                    @csrf
                    <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="menu_id" id="input-id" type="text"   value="{{ old('id', $editMenu->menu_id) }}" hidden/>

                   
                     
                    <div class="pl-lg-4">
                        <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Title</label>
                            <input type="text" name="title" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('') }}" value="{{$editMenu->title}}" required autofocus>

                        </div>
                      

                        <div class="form-group{{ $errors->has('position') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Position</label>
                            <input type="text" name="position" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('position') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('') }}" value="{{$editMenu->position}}" required autofocus>

                        </div>
                        
                        <div class="form-group{{ $errors->has('url') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">URL</label>
                            <input type="text" name="url" id="input-name"
                                class="form-control  form-control-alternative{{ $errors->has('url') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('') }}" value="{{$editMenu->url}}" required autofocus>

                        </div>

                        <div class="text-center">
                            <a href="{{ route('menu.listMenu') }}"  class="btn btn-warning mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>
@endsection

@push('js')
    <script>

    </script>
    @endpush
