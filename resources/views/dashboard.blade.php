@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-default card-header-icon">
              <!--<div class="card-icon">
                <i class="material-icons">person</i>
              </div>
              <p class="card-category">Users Number</p>
               <h3 class="card-title">{{ $res['u']}}</h3>
           
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons text-default">arrow_right_alt</i>
                <a href="{{ route('user.listUser') }}">List Users</a>
              </div>-->
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="material-icons ">star</i>
              </div>
             
            
           
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            
            
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
          <div class="card card-stats">
            
            
          </div>
        </div>
      </div>
      
      <div class="row">
        
        <div class="col-lg-6 col-md-12">
          
           
            
              
            </div>
          </div>
       
          <div class="col-lg-6 col-md-12">
          <div class="card">
           
              
                
            
          </div>
        </div>
        
        <div class="col-lg-12 col-md-12">
     
        
        
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script>
 
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();
    });
  </script>
@endpush