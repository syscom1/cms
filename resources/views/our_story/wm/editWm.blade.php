@extends('layouts.app', [
'class' => '',
'activePage' => 'wmlist', 'titlePage' => __('Edit Banner')])

@section('content')
<div class="content">

@if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
</br>

    <div class="container-fluid">
    <div class="card-header">

        <div class="row">

            <div class="col-md-12">
                <form method="post" action="{{ route('wm.updatewm') }}" autocomplete="off"  enctype="multipart/form-data">
                    @csrf
                    <input class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" id="input-id" type="text"   value="{{ old('id', $wm->id) }}" hidden/>

                    <div class="pl-lg-4">

                      

                         <div class="form-group{{ $errors->has('title') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Title </label>
                            <br>
                            <input type="text" name="title" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' Enter Title') }}" value="{{ $wm->title}}" required autofocus>

                        </div>


                        <div class="form-group{{ $errors->has('caption') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name">Caption </label>
                            <br>
                            <input type="text" name="caption" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('caption') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' Enter caption') }}" value="{{ $wm->caption}}" required autofocus>

                        </div>

                        <div class="text-center">
                            <a href="{{ route('wm.list') }}"  class="btn btn-warning mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>
@endsection

@push('js')
<script>
$('.datetimepicker').datetimepicker({
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
    },
    format : "DD-MM-YYYY"
});
   var loadFile = function(event) {
    //   $('#output').prop('src', null);

      console.log(event)
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };
</script>
    @endpush
