@extends('layouts.app', ['activePage' => 'ListTeam', 'titlePage' => __('Team Adding')])


@section('content')

<div class="content">
    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif

    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif
</br>


    <div class="container-fluid">
         
        <div class="card">
        <div class="card-header card-header-text card-header-success text-right">
        <div class="card-text ">
              <h4 class="card-title">Add New Team</h4>
            </div>
            
          
            
            </div>
            <div class="card-body">
            
            <div class="col-md-12">
                <form method="post" action="{{ route('about.addNewTeam') }}" autocomplete="off"  enctype="multipart/form-data">
                    @csrf
                    <div class="pl-lg-4">
                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Name</label>
                            <br>
                            <input type="text" name="team_name" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('news_title') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('team_name') }}" required >

                        </div>

                      

                        <div class="form-group{{ $errors->has('caption') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-name" >Caption</label>
                            <br>
                            <input type="text" name="team_caption" id="input-name"
                                class="form-control   form-control-alternative{{ $errors->has('news_caption') ? ' is-invalid' : '' }}"
                                placeholder="{{ __(' ') }}" value="{{ old('team_caption') }}" required >

                        </div>
                        
                        <img id="output" width="50%"/>

                       <div class="form-group">

            {{-- <img  id="output" src="{{asset('').$editCategory->category_image}}" width="50%"/> --}}
            <label class="form-control-label" for="input-name" >Image </label>

                        </div>
                          <div class="form-group">

    <label for="exampleInputFile" class="bmd-label-floating">
                                    <p  class="btn btn-success mt-4">
        Click to add image</p>
</label>
    <input type="file" class="form-control-file" name="team_image"  id="exampleInputFile" onchange="loadFile(event)"  />
   </div> 
                    



                </div> 
                        <div class="text-center">
                            <a href="{{ route('about.listTeam') }}"  class="btn btn-primary mt-4">{{ __('Back') }}</a>

                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    </br>
</div>

@endsection
@push('js')

<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };
</script>
@endpush
@push('js')
                     <script>
                         CKEDITOR.replace( 'editor' );
                   CKEDITOR.replace( 'editor1' );
                   </script>

<script>
 var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
};
    
           
              
    </script>
    @endpush