@extends('layouts.app', ['activePage' => 'AboutTeamInfo', 'titlePage' => 'About'])


@section('content')

<div class="content">
 
    @if (count($errors) > 0)
    @if($errors->any())
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{$errors->first()}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>
 @endif
@endif 
</br>


    <div class="container-fluid">
        <div class="card">
        <div class="card-header card-header-text card-header-success text-right">
        <div class="card-text ">
              <h4 class="card-title">Update Our Team</h4>
            </div>
            </div>
            <div class="card-body">

            <div class="col-md-12">
<form method="post" action="{{ route('about.updateTeamInfo') }}" autocomplete="off"  enctype="multipart/form-data"  class="needs-validation" novalidate>
                    @csrf
<div>
         <input type="hidden" name="id" value="{{$info->id}}" />

</div> 
<div class="pl-lg-4">
    @if ( Session::get('err') )
    <div class="alert alert-success alert-dismissible fade show" style="direction: ltr" role="alert">
        {{ Session::get('err') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          </div>            
    @endif
<div class="form-group">
             <label style="right: 0;" class="right">About</label>

     <textarea name="info_caption" id="editor" class="editor" rows="10" cols="80" required>
               {{$info->info_caption}}
            </textarea>
 
</div>


    {{-- <div class="form-group">
             <label style="right: 0;" class="right">About FR</label>

     <textarea name="info_about_us_fr" id="editor1" class="editor" rows="10" cols="80" required>
               {{$editInfo->info_about_us_fr}}
            </textarea>
 
    </div> --}}
                
               
    {{-- <!-- <div class="form-group">
            <img  id="output" src="{{asset('').$editInfo->info_image1}}" width="25%"/>

    </div> --}}
{{--     
<div class="form-group">
    <label for="exampleInputFile" class="bmd-label-floating">
    <p  class="btn btn-success mt-4">Add image 1</p>
    </label>
    <input type="file" class="form-control-file" name="info_image1"  id="exampleInputFile" onchange="loadFile(event)"  />
</div>  -->
   --}}
   
    <div class="text-center">
        <button type="submit"  id="submit-all" class="btn btn-success mt-4">Save</button>

 
    </div>

</div>
</form>
                </div> 
              </div>
               </div>    
               </div>    
               </div>
@endsection
@push('js')
                     <script>
                         CKEDITOR.replace( 'editor' );
                   CKEDITOR.replace( 'editor1' );
 
                  
     var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
      URL.revokeObjectURL(output.src) // free memory
    }
  };


                   
  
   </script>    

    @endpush