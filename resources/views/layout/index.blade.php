<!DOCTYPE html>
<html style="font-size: 16px;">
  <head>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Web Software Developers, Key Features, Data Visualization, IT solutions, IT solutions, Web Analytics, We are directly involved in t​he process, Get in Touch!">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Home</title>
    <link rel="stylesheet" href={{ asset('css/nicepage.css') }} media="screen">
<link rel="stylesheet" href={{ asset('css/Home.css') }} media="screen">
    <script class="u-script" type="text/javascript" src={{ asset('js/jquery.js') }} defer=""></script>
    <script class="u-script" type="text/javascript" src={{ asset('js/nicepage.js') }} defer=""></script>
    <meta name="generator" content="Nicepage 4.0.3, nicepage.com">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Barlow:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Barlow:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i">
    
    
    
    
    
    
    
    
    
    <script type="application/ld+json">{
		"@context": "http://schema.org",
		"@type": "Organization",
		"name": "Site2",
		"logo": "images/logo-white3.png"
}</script>
    <meta name="theme-color" content="#29b7dd">
    <meta property="og:title" content="Home">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
  </head>
  <body class="u-body u-overlap u-overlap-contrast u-overlap-transparent">
    <header class="u-clearfix u-custom-color-1 u-header u-sticky u-sticky-915a u-header" id="sec-2485"><div class="u-clearfix u-sheet u-sheet-1">
        <a href="/Home" data-page-id="704516926" class="u-align-left u-image u-logo u-image-1" data-image-width="595" data-image-height="283" title="Home">
          <img src={{ asset('files/images/logo-white3.png') }} class="u-logo-image u-logo-image-1">
        </a>
@include('layout.header')
</div><style class="u-sticky-style" data-style-id="915a">.u-sticky-fixed.u-sticky-915a:before, .u-body.u-sticky-fixed .u-sticky-915a:before {
borders: top right bottom left !important
}</style></header>
    
    
    <span style="height: 64px; width: 64px; margin-left: 0px; margin-right: auto; margin-top: 0px; background-image: none; right: 20px; bottom: 20px" class="u-back-to-top u-icon u-icon-circle u-opacity u-opacity-85 u-palette-1-base u-spacing-15" data-href="#">
        <svg class="u-svg-link" preserveAspectRatio="xMidYMin slice" viewBox="0 0 551.13 551.13"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-1d98"></use></svg>
        <svg class="u-svg-content" enable-background="new 0 0 551.13 551.13" viewBox="0 0 551.13 551.13" xmlns="http://www.w3.org/2000/svg" id="svg-1d98"><path d="m275.565 189.451 223.897 223.897h51.668l-275.565-275.565-275.565 275.565h51.668z"></path></svg>
    </span>
  </body>
</html>