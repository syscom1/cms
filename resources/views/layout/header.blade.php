<html>

    <nav class="u-menu u-menu-dropdown u-offcanvas u-menu-1">
        <div class="menu-collapse" style="font-size: 1rem; letter-spacing: 0px;">
          <a class="u-button-style u-custom-left-right-menu-spacing u-custom-padding-bottom u-custom-top-bottom-menu-spacing u-nav-link u-text-active-palette-1-base u-text-hover-palette-2-base" href="#">
            <svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#menu-hamburger"></use></svg>
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><defs><symbol id="menu-hamburger" viewBox="0 0 16 16" style="width: 16px; height: 16px;"><rect y="1" width="16" height="2"></rect><rect y="7" width="16" height="2"></rect><rect y="13" width="16" height="2"></rect>
</symbol>
</defs></svg>
          </a>
        </div>
        <div class="u-custom-menu u-nav-container">
          <ul class="u-nav u-spacing-50 u-unstyled u-nav-1"><li class="u-nav-item"><a class="u-border-2 u-border-active-custom-color-1 u-border-hover-palette-4-light-2 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-palette-4-dark-2 u-text-hover-palette-4-light-2 u-text-white" href="/Home" style="padding: 10px 0px;">Home</a>
</li><li class="u-nav-item"><a class="u-border-2 u-border-active-custom-color-1 u-border-hover-palette-4-light-2 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-palette-4-dark-2 u-text-hover-palette-4-light-2 u-text-white" href="/About-Us" style="padding: 10px 0px;">About Us</a>
</li><li class="u-nav-item"><a class="u-border-2 u-border-active-custom-color-1 u-border-hover-palette-4-light-2 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-palette-4-dark-2 u-text-hover-palette-4-light-2 u-text-white" href="/Services" style="padding: 10px 0px;">Services</a>
</li><li class="u-nav-item"><a class="u-border-2 u-border-active-custom-color-1 u-border-hover-palette-4-light-2 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-palette-4-dark-2 u-text-hover-palette-4-light-2 u-text-white" href="/Contact-Us" style="padding: 10px 0px;">Contact Us</a>
</li>
<li class="u-nav-item"><a class="u-border-2 u-border-active-custom-color-1 u-border-hover-palette-4-light-2 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-palette-4-dark-2 u-text-hover-palette-4-light-2 u-text-white" href="/OurWork" style="padding: 10px 0px;color:white">Our Work</a>
</li>
<li class="u-nav-item"><a class="u-border-2 u-border-active-custom-color-1 u-border-hover-palette-4-light-2 u-border-no-left u-border-no-right u-border-no-top u-button-style u-nav-link u-text-active-palette-4-dark-2 u-text-hover-palette-4-light-2 u-text-white" href="/News" style="padding: 10px 0px;color:white">News</a>
</li>
</ul>
        </div>
        <div class="u-custom-menu u-nav-container-collapse">
          <div class="u-black u-container-style u-inner-container-layout u-opacity u-opacity-95 u-sidenav">
            <div class="u-inner-container-layout u-sidenav-overflow">
              <div class="u-menu-close"></div>
              <ul class="u-align-center u-nav u-popupmenu-items u-unstyled u-nav-3"><li class="u-nav-item"><a class="u-button-style u-nav-link" href="/Home" style="padding: 10px 0px;">Home</a>
</li><li class="u-nav-item"><a class="u-button-style u-nav-link" href="/About-Us" style="padding: 10px 0px;">About Us</a>
</li><li class="u-nav-item"><a class="u-button-style u-nav-link" href="/Services" style="padding: 10px 0px;">Services</a>
</li>
<li class="u-nav-item"><a class="u-button-style u-nav-link" href="/Contact-Us" style="padding: 10px 0px;">Contact Us</a>
</li>
<li class="u-nav-item"><a class="u-button-style u-nav-link" href="/OurWork" style="padding: 10px 0px;">Our Work</a>
</li>
<li class="u-nav-item"><a class="u-button-style u-nav-link" href="/News" style="padding: 10px 0px;">News</a>
</li></ul>
            </div>
          </div>
          <div class="u-black u-menu-overlay u-opacity u-opacity-70"></div>
        </div>
      </nav>

</html>