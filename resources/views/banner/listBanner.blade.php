@extends('layouts.app', ['activePage' => 'listBanner', 'titlePage' => __('Banners List')])


@section('content')
</br>
</br>
</br>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="shadow p-3 mb-5 bg-white rounded">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col-sm-8">
                                    <h3 class="mb-0">{{ __('Banners') }}</h3>
                                </div>
                                <div class="col-sm-4">
                                <div class="col-md-4 offset-md-4 clearfix">
                                    <a href="{{ route('banner.addBanner') }}" class="btn btn-primary btn-lg " role="button">{{ __('Add  Banner') }}</a>
                                </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            
        @if (count($errors) > 0)
        @if($errors->any())
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{$errors->first()}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
        @endif
    @endif
                            @if (session('status'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                        </div>

                        <div class="table-responsive">
                            <table class="table align-items-center table-flush datatable">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">{{ __('#') }}</th>
                                    
                                        
                                         <th scope="col">{{ __('Title') }}</th>
                                        <th scope="col">{{ __('Caption') }}</th>
                                        <th scope="col">{{ __('Photo') }}</th>
                                        <th scope="col">{{ __('Link') }}</th>
                                


                                        
                                        <th scope="col">{{ __('Action') }}</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($listBanner as $key=>$user)
                                        <tr>
                                            <td>{{ $key+1}} </td>
                                       
                                            
                                            <td>{{ $user->title}} </td>
                                            <td>{{ $user->caption}} </td> 
                                            <td>
                                                <button id="click{{ $user->id }}" class="btn btn-primary">View</button>
                                                @push('js')
                                                <script>
                                                   $('#click{{ $user->id }}').on('click',function(){
                                                       swal.fire({
                                                        imageUrl: '{{ asset($user->banner_img) }}',
                                                        imageHeight: 200,
                                                        imageAlt: 'A tall image'
                                                       })
                                                   })
                                                </script>
                                            @endpush</td> 
                                            <td>{{ $user->banner_url}} </td> 

                                          

                                            
                                            
                                            <td>
                                            <a href="deleteBanner/{{$user->banner_id }}" 
                                               onclick="return confirm('Are you sure you want to delete this Banner?')"   class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                                <i class="material-icons">delete</i></a>
                                           
                                               
                                               @if($user->banner_status == 0)
                                               <a href="statusBanner/{{$user->banner_id}}/1"  class="btn btn-primary btn-fab btn-fab-mini btn-round"
                                               onclick="return confirm('Are you sure you want to restore this Banner?')"  role="button" aria-disabled="true">
                                                <i class="material-icons">refresh</i></a>
                                                   @endif
   
                                                   @if($user->banner_status == 1)
   
                                                <a href="statusBanner/{{$user->banner_id}}/0" 
                                               onclick="return confirm('Are you sure you want to hide this Banner?')"   class="btn btn-danger btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                                <i class="material-icons">close</i></a>
                                                @endif
    
                                             
                                            
                                            <a href="editBanner/{{$user->banner_id}}" class="btn btn-success btn-fab btn-fab-mini btn-round" role="button" aria-disabled="true">
                                             <i class="material-icons">edit</i></a>

                                               
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>



                       
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('js')
    <script>
$('.datatable').DataTable();

    </script>
    @endpush