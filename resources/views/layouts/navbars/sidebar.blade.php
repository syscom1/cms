<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="#" class="simple-text logo-normal">
      {{ __('Life Style ') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
   
      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
        <!-- <a class="nav-link" data-toggle="collapse" href="#laravelExample" aria-expanded="true">-->
        <!--  <i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>-->
        <!--  <p>{{ __('User Control') }}-->
        <!--    <b class="caret"></b>-->
        <!--  </p>-->
        <!--</a> -->
        <div class="collapse show" id="laravelExample">
          <ul class="nav">
            <!-- <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('profile.edit') }}">
                <span class="sidebar-mini"> UP </span>
                <span class="sidebar-normal">{{ __('User profile') }} </span>
              </a>
            </li> -->
            <!-- <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('user.index') }}">
                <span class="sidebar-mini"> UM </span>
                <span class="sidebar-normal"> {{ __('User Management') }} </span>
              </a>
            </li> -->
          </ul>
        </div>
        
        <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>

      <li class="nav-item{{ $activePage == 'listMenu' || $activePage == 'addMenu' || $activePage =='editMenu' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('menu.listMenu') }}">
          <i class="material-icons">menu</i>
            <p>{{ __('Menu') }}</p>
        </a>
      </li>


      <li class="nav-item{{ $activePage == 'listUser' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('user.listUser') }}">
          <i class="material-icons">person</i>
            <p>{{ __('Admins') }}</p>
        </a>
      </li>


      <!-- Start Dropdown of Home-->

<li class="nav-item {{ ($activePage == 'editInfo1' || $activePage == 'listBanner') ? ' active' : '' }}">
        <a class="nav-link  {{ ($activePage == 'editInfo1' || $activePage == 'listBanner') ? '' : 'collapsed' }} " data-toggle="collapse" href="#laravelExample2" aria-expanded=" {{ ($activePage == 'editInfos1' || $activePage == 'listBanner') ? 'true' : 'false'}}"> 
        <i class="material-icons">home</i>
         <p>Home
           <b class="caret"></b>
         </p>
          </a>
        <div class="collapse {{ ($activePage == 'editInfo1' || $activePage == 'listBanner' || $activePage=='OurWork') ? 'show' : 'hide' }} " id="laravelExample2">
          <ul class="nav"> 

          <li class="nav-item{{ $activePage == 'listBanner' ? ' active' : '' }}">
              <a class="nav-link" href="{{route('banner.listBanner') }}">
                <span class="sidebar-mini"> BN </span>
                <span class="sidebar-normal">Banner </span>
              </a>
            </li> 


            {{-- <li class="nav-item{{ $activePage == 'editInfo1' ? ' active' : '' }}">
              <a class="nav-link" href="{{route('infos1.editInfos1') }}">
                <span class="sidebar-mini"> ROI </span>
                <span class="sidebar-normal">ROI </span>
              </a>
            </li> 
       
         
            <li class="nav-item{{ $activePage == 'OurWork' ? ' active' : '' }}">
              <a class="nav-link" href="{{route('banner.listourwork') }}">
                <span class="sidebar-mini">OW</span>
                <span class="sidebar-normal">Our Work </span>
              </a>
            </li> 
             --}}
      
          </ul>
        </div>
        </li>

 <!-- End Dropdown -->



      <!-- <li class="nav-item{{ $activePage == 'listBanner' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('banner.listBanner') }}">
          <i class="material-icons">language</i>
            <p>{{ __('Banners') }}</p>
        </a>
      </li>

      <li class="nav-item{{ $activePage == 'editInfos1' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('infos1.editInfos1') }}">
          <i class="material-icons">info</i>
            <p>Home Content</p>
        </a>
      </li> -->

      

      
      
      
    

      
<!-- Start Dropdown of Services-->

 <li class="nav-item {{ ($activePage == 'storylistBanner' || $activePage == 'walist' || $activePage == 'wmlist') ? ' active' : '' }}">
        <a class="nav-link  {{ ($activePage == 'storylistBanner' || $activePage == 'listServices' || $activePage == 'editServicesIntro') ? '' : 'collapsed' }} " data-toggle="collapse" href="#laravelExample5" aria-expanded=" {{ ($activePage == 'listCategory' || $activePage == 'walist' || $activePage == 'editServicesIntro') ? 'true' : 'false'}}"> 
        <i class="material-icons">auto_stories</i>
         <p>Our story
           <b class="caret"></b>
         </p>
          </a>
        <div class="collapse {{ ($activePage == 'storylistBanner' || $activePage == 'walist' || $activePage == 'wmlist') ? 'show' : 'hide' }} " id="laravelExample5">
          <ul class="nav"> 
            <li class="nav-item{{ $activePage == 'storylistBanner' ? ' active' : '' }}">
              <a class="nav-link" href="{{route('story.listBanner') }}">
                <span class="sidebar-mini"> BN </span>
                <span class="sidebar-normal">Banner </span>
              </a>
            </li> 
       
            <li class="nav-item{{ $activePage == 'walist' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('wa.list') }}">
          <span class="sidebar-mini"> WA </span>
                <span class="sidebar-normal">Who we are </span>
        </a>
      </li>

      <li class="nav-item{{ $activePage == 'wmlist' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('wm.list') }}">
          <span class="sidebar-mini"> WM </span>
                <span class="sidebar-normal">What make </span>
        </a>
      </li>
            
      
          </ul>
        </div>
        </li> 

 <!-- End Dropdown -->





     

<li class="nav-item {{ ($activePage == 'listCategory' || $activePage == 'listSubcategory' || $activePage == 'AboutlistBanner') ? ' active' : '' }}">
        <a class="nav-link  {{ ($activePage == 'AboutTeamInfo' || $activePage == 'ListTeam' || $activePage == 'AboutlistBanner') ? '' : 'collapsed' }} " data-toggle="collapse" href="#laravelExample3" aria-expanded=" {{ ($activePage == 'editInfos1' || $activePage == 'listBanner') ? 'true' : 'false'}}"> 
        <i class="material-icons">category</i>
         <p>Our brands
           <b class="caret"></b>
         </p>
          </a>
        <div class="collapse {{ ($activePage == 'listCategory' || $activePage == 'listSubcategory' || $activePage == 'AboutlistBanner') ? 'show' : 'hide' }} " id="laravelExample3">
          <ul class="nav"> 

          <li class="nav-item{{ $activePage == 'listCategory' ? ' active' : '' }}">
              <a class="nav-link" href="{{route('category.listCategory') }}">
                <i class="material-icons">category</i>
                <p>{{ __('Category') }}</p>
              </a>
            </li> 


            <li class="nav-item{{ $activePage == 'listSubcategory' ? ' active' : '' }}">
              <a class="nav-link" href="{{route('Subcategory.listSubcategory') }}">
                <i class="material-icons">subtitles</i>
                <p>{{ __('Sub-Category') }}</p>
              </a>
            </li> 
       
         
      
          </ul>
        </div>
        </li>

      


        <li class="nav-item{{ ($activePage == 'listContact' || $activePage=='addContact' || $activePage=='editContact') ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('contact.listContact') }}">
            <i class="material-icons">contact_support</i>
              <p>{{ __('Contact') }}</p>
          </a>
        </li>
     
        {{-- <li class="nav-item{{ $activePage == 'listUser' ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('contact.listContact') }}">
            <i class="material-icons">contact_support</i>
              <p>{{ __('Contact') }}</p>
          </a>
        </li> --}}
        

     
      {{-- <li class="nav-item{{ $activePage == 'ListNews' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('news.listNews') }}">
          <i class="material-icons">feed</i>
            <p>{{ __('News') }}</p>
        </a>
      </li> --}}
      
      {{-- <li class="nav-item{{ $activePage == 'listOur' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('ourwork.listOur') }}">
          <i class="material-icons">work</i>
            <p>{{ __('Our Work') }}</p>
        </a>
      </li>

      
  
      <li class="nav-item{{ $activePage == 'listContact' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('contact.listContact') }}">
          <i class="material-icons">contact_support</i>
            <p>{{ __('Contact') }}</p>
        </a>
      </li>
       --}}
      

          
      
    </ul>
  </div>
</div>
