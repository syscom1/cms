<?php


// header("Cache-Control: no-cache, must-revalidate");
// header('Access-Control-Allow-Origin:  *');
// header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
// header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');




use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Auth::routes();
 
Route::get('/getBanners', 'App\Http\Controllers\App\MobileController@getBanners');
Route::get('/getCategory', 'App\Http\Controllers\App\MobileController@getCategory');
Route::get('/getSubcategory', 'App\Http\Controllers\App\MobileController@getSubcategory');
Route::get('/getNewArrivals', 'App\Http\Controllers\App\MobileController@getNewArrivals');
Route::get('/getSpecials', 'App\Http\Controllers\App\MobileController@getSpecials');

Route::get('/getBrands', 'App\Http\Controllers\App\MobileController@getBrands');
Route::get('/getBrandList', 'App\Http\Controllers\App\MobileController@getBrandList');
Route::get('/getProductBrands', 'App\Http\Controllers\App\MobileController@getProductBrands');
Route::post('/checkProductCart', 'App\Http\Controllers\App\MobileController@checkProductCart');



Route::get('/getProduct', 'App\Http\Controllers\App\MobileController@getProduct');
Route::get('/getProductSub', 'App\Http\Controllers\App\MobileController@getProductSub');
 Route::get('/getProductDetails', 'App\Http\Controllers\App\MobileController@getProductDetails');
 
 Route::get('/getInfoData', 'App\Http\Controllers\App\MobileController@getInfoData');

 Route::post('/login', 'App\Http\Controllers\App\MobileController@login');
 Route::post('/register', 'App\Http\Controllers\App\MobileController@register');
 Route::post('/updateProfile', 'App\Http\Controllers\App\MobileController@updateProfile');

 Route::get('/getOrders', 'App\Http\Controllers\App\MobileController@getOrders');
 Route::post('/addOrder', 'App\Http\Controllers\App\MobileController@addOrder');
 Route::post('/placeOrder', 'App\Http\Controllers\App\MobileController@placeOrder');

 
  Route::get('/getCities', 'App\Http\Controllers\App\MobileController@getCities');
  Route::get('/getAddresses', 'App\Http\Controllers\App\MobileController@getAddresses');
  Route::get('/getRelatedProduct', 'App\Http\Controllers\App\MobileController@getRelatedProduct');
  Route::get('/getSearchProduct', 'App\Http\Controllers\App\MobileController@getSearchProduct');
  Route::post('/addAddress', 'App\Http\Controllers\App\MobileController@addAddress');
  Route::post('/updateAddress', 'App\Http\Controllers\App\MobileController@updateAddress');


  Route::get('/getDboyOrders', 'App\Http\Controllers\App\MobileController@getDboyOrders');
    Route::get('/assignStatus', 'App\Http\Controllers\App\MobileController@assignStatus');


   Route::post('/makePassword', 'App\Http\Controllers\App\MobileController@makePassword');
      Route::post('/forgetPassword', 'App\Http\Controllers\App\MobileController@forgetPassword');





