<?php

use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
// use App;
// App::setLocale('ar');

 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

   


Route::get('/', function () {
	//to new page website
    return redirect()->route('login');
});

Route::get('/admin', function () {
	//to new page website
    return redirect()->route('login');
});

// Route::get('/admin', function () {
//     return redirect()->route('login');
// });






/* From validate */
 Route::post('/contact-form', 'App\Http\Controllers\WebsiteController@storeForm')->name('contactForm.save');;








//end route website




Route::get('/reset', function(){
	Session::flush();
});

Route::get('/clear-cash', function(){
	Session::flush();
});

Auth::routes();
//Route::get('/home', [App\Http\Controllers\ProjectController::class, 'listCategory'])->name('home');

Auth::routes();
//guest middleware aw bala middleware
Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home')->middleware('auth');


Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);





	Route::prefix('admin')->group(function () {
//Start Contact Route -------------------------------------------------------------------->

Route::get('listContact', ['as' => 'contact.listContact', 'uses' => 'App\Http\Controllers\ContactController@listContact']);
    //hay lal view
	Route::get('addContact', ['as' => 'contact.addContact', 'uses' => 'App\Http\Controllers\ContactController@addContact']);
	//hay lal action
	Route::post('addNewContact',['as' => 'contact.addNewContact', 'uses' => 'App\Http\Controllers\ContactController@addNewContact']);

	Route::get('editContact/{id}', ['as' => 'contact.editContact', 'uses' => 'App\Http\Controllers\ContactController@editContact']);
	Route::post('updateContact',['as' => 'contact.updateContact', 'uses' => 'App\Http\Controllers\ContactController@updateContact']);
    Route::get('statusContact/{id}/{status}',['as' => 'contact.statusContact', 'uses' => 'App\Http\Controllers\ContactController@statusContact']);
	Route::get('deleteContact/{id}',['as' => 'contact.deleteContact', 'uses' => 'App\Http\Controllers\ContactController@deleteContact']);




//Start news Route -------------------------------------------------------------------->

	Route::get('listNews', ['as' => 'news.listNews', 'uses' => 'App\Http\Controllers\NewsController@listNews']);
    //hay lal view
	Route::get('addNews', ['as' => 'news.addNews', 'uses' => 'App\Http\Controllers\NewsController@addNews']);
	//hay lal action
	Route::post('addnewNews', ['as' => 'news.addnewNews', 'uses' => 'App\Http\Controllers\NewsController@addnewNews']);

	Route::get('editNews/{id}', ['as' => 'news.editNews', 'uses' => 'App\Http\Controllers\NewsController@editNews']);
	Route::post('updateNews',['as' => 'news.updateNews', 'uses' => 'App\Http\Controllers\NewsController@updateNews']);
	Route::get('deleteNews/{id}',['as' => 'news.deletenews', 'uses' => 'App\Http\Controllers\NewsController@deleteNews']);





// Start Menu Route ----------------------------------------------------------------------->

     Route::get('listMenu', ['as' => 'menu.listMenu', 'uses' => 'App\Http\Controllers\MenuController@listMenu']);
     //hay lal view
	 Route::get('addMenu', ['as' => 'menu.addMenu', 'uses' => 'App\Http\Controllers\MenuController@addMenu']);
	 //hay lal action
	 Route::post('addNewMenu',['as' => 'menu.addNewMenu', 'uses' => 'App\Http\Controllers\MenuController@addNewMenu']);

	 Route::get('editMenu/{id}', ['as' => 'menu.editMenu', 'uses' => 'App\Http\Controllers\MenuController@editMenu']);
	 Route::post('updateMenu',['as' => 'menu.updateMenu', 'uses' => 'App\Http\Controllers\MenuController@updateMenu']);
     Route::get('statusMenu/{id}/{status}',['as' => 'menu.statusmenu', 'uses' => 'App\Http\Controllers\MenuController@statusMenu']);
	 Route::get('deleteMenu/{id}',['as' => 'menu.deleteMenu', 'uses' => 'App\Http\Controllers\MenuController@deleteMenu']);



	// Start Our Work Route ----------------------------------------------------------------------->

	Route::get('listOur', ['as' => 'ourwork.listOur', 'uses' => 'App\Http\Controllers\OurController@listOur']);
	//hay lal view
	Route::get('addOur', ['as' => 'ourwork.addOur', 'uses' => 'App\Http\Controllers\OurController@addOur']);
	//hay lal action
	Route::post('addNewOur',['as' => 'ourwork.addNewOur', 'uses' => 'App\Http\Controllers\OurController@addNewOur']);

	Route::get('editOur/{id}', ['as' => 'ourwork.editOur', 'uses' => 'App\Http\Controllers\OurController@editOur']);
	Route::post('updateOur',['as' => 'ourwork.updateOur', 'uses' => 'App\Http\Controllers\OurController@updateOur']);
	Route::get('deleteOur/{id}',['as' => 'ourwork.deleteOur', 'uses' => 'App\Http\Controllers\OurController@deleteOur']);

	Route::get('addImg/{id}', ['as' => 'album.addImg', 'uses' => 'App\Http\Controllers\OurController@addImg']);
	Route::post('uploadImg',['as' => 'album.uploadImg', 'uses' => 'App\Http\Controllers\OurController@uploadImg']);
	Route::get('fetchImg', ['as' => 'album.fetchImg', 'uses' => 'App\Http\Controllers\OurController@fetchImg']);
	Route::post('addImg/deleteImg', ['as' => 'album.deleteImg', 'uses' => 'App\Http\Controllers\OurController@deleteImg']);
	Route::post('addImg/sortImages', ['as' => 'album.sortImages', 'uses' => 'App\Http\Controllers\OurController@sortImages']);
	Route::get('statusAlbum/{id}/{status}',['as' => 'album.statusAlbum', 'uses' => 'App\Http\Controllers\OurController@statusAlbum']);




	// Start Category and Sub-Category Route-------------------------------------------------------------->

	 Route::get('listCategory', ['as' => 'category.listCategory', 'uses' => 'App\Http\Controllers\CategoryController@listCategory']);
	 Route::get('addCategory', ['as' => 'category.addCategory', 'uses' => 'App\Http\Controllers\CategoryController@addCategory']);
	 Route::post('addNewCategory',['as' => 'category.addNewCategory', 'uses' => 'App\Http\Controllers\CategoryController@addNewCategory']);
	 Route::get('editCategory/{id}', ['as' => 'category.editCategory', 'uses' => 'App\Http\Controllers\CategoryController@editCategory']);
	 Route::post('updateCategory',['as' => 'category.updateCategory', 'uses' => 'App\Http\Controllers\CategoryController@updateCategory']);
	 Route::get('statusCategory/{id}/{status}',['as' => 'category.statusCategory', 'uses' => 'App\Http\Controllers\CategoryController@statusCategory']);
	 Route::get('deleteCategory/{id}',['as' => 'category.deleteCategory', 'uses' => 'App\Http\Controllers\CategoryController@deleteCategory']);
	 Route::get('addImgCat/{id}', ['as' => 'category.addImg', 'uses' => 'App\Http\Controllers\CategoryController@addImg']);
	 Route::post('uploadImgCat',['as' => 'category.uploadImg', 'uses' => 'App\Http\Controllers\CategoryController@uploadImg']);
	 Route::get('fetchImgCat', ['as' => 'category.fetchImg', 'uses' => 'App\Http\Controllers\CategoryController@fetchImg']);
	 Route::post('/addImgCat/deleteCatImg', ['as' => 'category.deleteImg', 'uses' => 'App\Http\Controllers\CategoryController@deleteImg']);
	 Route::post('addImgCat/sortImages', ['as' => 'category.sortImages', 'uses' => 'App\Http\Controllers\CategoryController@sortImages']);
	 Route::get('statusAlbumCat/{id}/{status}',['as' => 'category.statusAlbum', 'uses' => 'App\Http\Controllers\CategoryController@statusAlbum']);


	 Route::get('listSubcategory', ['as' => 'Subcategory.listSubcategory', 'uses' => 'App\Http\Controllers\CategoryController@listSubcategory']);
	 Route::get('addSubcategory', ['as' => 'Subcategory.addSubcategory', 'uses' => 'App\Http\Controllers\CategoryController@addSubcategory']);
	 Route::post('addNewSubcategory',['as' => 'Subcategory.addNewSubcategory', 'uses' => 'App\Http\Controllers\CategoryController@addNewSubcategory']);
	 Route::get('editSubcategory/{id}', ['as' => 'Subcategory.editSubcategory', 'uses' => 'App\Http\Controllers\CategoryController@editSubcategory']);
	 Route::post('updateSubcategory',['as' => 'Subcategory.updateSubcategory', 'uses' => 'App\Http\Controllers\CategoryController@updateSubcategory']);
	 Route::get('statusSubcategory/{id}/{status}',['as' => 'Subcategory.statusSubcategory', 'uses' => 'App\Http\Controllers\CategoryController@statusSubcategory']);
	 Route::get('deleteSubcategory/{id}',['as' => 'Subcategory.deleteSubcategory', 'uses' => 'App\Http\Controllers\CategoryController@deleteSubcategory']);
	 Route::get('addImgSub/{id}', ['as' => 'Subcategory.addImg', 'uses' => 'App\Http\Controllers\CategoryController@addImgSub']);
	 Route::post('uploadImgSub',['as' => 'Subcategory.uploadImg', 'uses' => 'App\Http\Controllers\CategoryController@uploadImgSub']);
	 Route::get('fetchImgSub', ['as' => 'Subcategory.fetchImg', 'uses' => 'App\Http\Controllers\CategoryController@fetchImgSub']);
	 Route::post('/addImgSub/deleteSubImg', ['as' => 'Subcategory.deleteImg', 'uses' => 'App\Http\Controllers\CategoryController@deleteImgSub']);
	 Route::post('addImgSub/sortImages', ['as' => 'Subcategory.sortImages', 'uses' => 'App\Http\Controllers\CategoryController@sortImagesSub']);
	 Route::get('statusAlbumSub/{id}/{status}',['as' => 'Subcategory.statusAlbum', 'uses' => 'App\Http\Controllers\CategoryController@statusAlbumSub']);

// Start Banner Route-------------------------------------------------------------->

Route::get('listBanner', ['as' => 'banner.listBanner', 'uses' => 'App\Http\Controllers\BannerController@listBanner']);
Route::get('addBanner', ['as' => 'banner.addBanner', 'uses' => 'App\Http\Controllers\BannerController@addBanner']);
Route::post('addNewBanner',['as' => 'banner.addNewBanner', 'uses' => 'App\Http\Controllers\BannerController@addNewBanner']);
Route::get('editBanner/{id}', ['as' => 'banner.editBanner', 'uses' => 'App\Http\Controllers\BannerController@editBanner']);
Route::post('updateBanner',['as' => 'banner.updateBanner', 'uses' => 'App\Http\Controllers\BannerController@updateBanner']);
Route::get('deleteBanner/{id}',['as' => 'banner.deleteBanner', 'uses' => 'App\Http\Controllers\BannerController@deleteBanner']);
Route::get('statusBanner/{id}/{status}',['as' => 'banner.statusBanner', 'uses' => 'App\Http\Controllers\BannerController@statusBanner']);



// Start Home Our Work Route


    Route::get('homeourwork', ['as' => 'banner.listourwork', 'uses' => 'App\Http\Controllers\BannerController@listOur']);
	//hay lal view
	Route::get('homeaddOur', ['as' => 'banner.addOurHome', 'uses' => 'App\Http\Controllers\BannerController@addOurhome']);
	//hay lal action
	Route::post('homeaddNewOur',['as' => 'banner.addNewOurHome', 'uses' => 'App\Http\Controllers\BannerController@addnewourhome']);

	Route::get('editOurHome/{id}', ['as' => 'banner.editOurHome', 'uses' => 'App\Http\Controllers\BannerController@editOurHome']);
	Route::post('updateOurHome',['as' => 'banner.updateOurHome', 'uses' => 'App\Http\Controllers\BannerController@updateOurHome']);
	Route::get('deleteOurHome/{id}',['as' => 'banner.deleteOur', 'uses' => 'App\Http\Controllers\BannerController@deleteOurHome']);

	Route::get('addImgOur/{id}', ['as' => 'album.addImg', 'uses' => 'App\Http\Controllers\BannerController@addImg']);
	Route::post('uploadImgOur',['as' => 'album.uploadImg', 'uses' => 'App\Http\Controllers\BannerController@uploadImg']);
	Route::get('fetchImgOur', ['as' => 'album.fetchImg', 'uses' => 'App\Http\Controllers\BannerController@fetchImg']);
	Route::post('addImgOur/deleteOurImg', ['as' => 'album.deleteImg', 'uses' => 'App\Http\Controllers\BannerController@deleteImg']);
	Route::post('addImgOur/sortImages', ['as' => 'album.sortImages', 'uses' => 'App\Http\Controllers\BannerController@sortImages']);
	Route::get('statusAlbumOur/{id}/{status}',['as' => 'album.statusAlbum', 'uses' => 'App\Http\Controllers\BannerController@statusAlbum']);


// Start About Banner Route

Route::get('about/listBanner', ['as' => 'about.listBanner', 'uses' => 'App\Http\Controllers\AboutController@listBanner']);
Route::get('about/addBanner', ['as' => 'about.addBanner', 'uses' => 'App\Http\Controllers\AboutController@addBanner']);
Route::post('about/addNewBanner',['as' => 'about.addNewBanner', 'uses' => 'App\Http\Controllers\AboutController@addNewBanner']);
Route::get('about/editBanner/{id}', ['as' => 'about.editBanner', 'uses' => 'App\Http\Controllers\AboutController@editBanner']);
Route::post('about/updateBanner',['as' => 'about.updateBanner', 'uses' => 'App\Http\Controllers\AboutController@updateBanner']);
Route::get('about/deleteBanner/{id}',['as' => 'about.deleteBanner', 'uses' => 'App\Http\Controllers\AboutController@deleteBanner']);

// Start About Team Info Route

Route::get('about/editTeamInfo', ['as' => 'about.editTeamInfo', 'uses' => 'App\Http\Controllers\AboutController@editTeamInfo']);
Route::post('about/updateTeamInfo', ['as' => 'about.updateTeamInfo', 'uses' => 'App\Http\Controllers\AboutController@updateTeamInfo']); 


// Start About Banner Route

Route::get('about/listTeam', ['as' => 'about.listTeam', 'uses' => 'App\Http\Controllers\AboutController@listTeam']);
Route::get('about/addTeam', ['as' => 'about.addTeam', 'uses' => 'App\Http\Controllers\AboutController@addTeam']);
Route::post('about/addNewTeam',['as' => 'about.addNewTeam', 'uses' => 'App\Http\Controllers\AboutController@addNewTeam']);
Route::get('about/editTeam/{id}', ['as' => 'about.editTeam', 'uses' => 'App\Http\Controllers\AboutController@editTeam']);
Route::post('about/updateTeam',['as' => 'about.updateTeam', 'uses' => 'App\Http\Controllers\AboutController@updateTeam']);
Route::get('about/deleteTeam/{id}',['as' => 'about.deleteTeam', 'uses' => 'App\Http\Controllers\AboutController@deleteTeam']);





// Start Services Route-------------------------------------------------------------->

Route::get('listServices', ['as' => 'services.listServices', 'uses' => 'App\Http\Controllers\ServicesController@listServices']);
Route::get('addServices', ['as' => 'services.addServices', 'uses' => 'App\Http\Controllers\ServicesController@addServices']);
Route::post('addNewServices',['as' => 'services.addNewServices', 'uses' => 'App\Http\Controllers\ServicesController@addNewServices']);
Route::get('editServices/{id}', ['as' => 'services.editServices', 'uses' => 'App\Http\Controllers\ServicesController@editServices']);
Route::post('updateServices',['as' => 'services.updateServices', 'uses' => 'App\Http\Controllers\ServicesController@updateServices']);
Route::get('statusServices/{id}/{status}',['as' => 'services.statusServices', 'uses' => 'App\Http\Controllers\ServicesController@statusServices']);
Route::get('deleteServices/{id}',['as' => 'services.deleteServices', 'uses' => 'App\Http\Controllers\ServicesController@deleteServices']);

    Route::get('addvid', ['as' => 'album.addImg', 'uses' => 'App\Http\Controllers\ServicesController@addImg']);
	Route::post('uploadvid',['as' => 'album.uploadImg', 'uses' => 'App\Http\Controllers\ServicesController@uploadImg']);
	Route::get('fetchvid', ['as' => 'album.fetchImg', 'uses' => 'App\Http\Controllers\ServicesController@fetchImg']);
	Route::post('addvid/deletevid', ['as' => 'album.deleteImg', 'uses' => 'App\Http\Controllers\ServicesController@deleteImg']);
	Route::post('addvid/sortvid', ['as' => 'album.sortImages', 'uses' => 'App\Http\Controllers\ServicesController@sortImages']);
	Route::get('statusAlbumOur/{id}/{status}',['as' => 'album.statusAlbum', 'uses' => 'App\Http\Controllers\ServicesController@statusAlbum']);


// Start User Route-------------------------------------------------------------->

Route::get('listUserAddress', ['as' => 'user.listUserAddress', 'uses' => 'App\Http\Controllers\ClientController@listUserAddress']);
Route::get('listUserOrders', ['as' => 'user.listUserOrders', 'uses' => 'App\Http\Controllers\ClientController@listUserOrders']);
Route::get('listClient', ['as' => 'user.listClient', 'uses' => 'App\Http\Controllers\ClientController@listClient']);

Route::get('listUser', ['as' => 'user.listUser', 'uses' => 'App\Http\Controllers\ProjectController@listUser']);
Route::get('addUser', ['as' => 'user.addUser', 'uses' => 'App\Http\Controllers\ProjectController@addUser']);
Route::post('addNewUser',['as' => 'user.addNewUser', 'uses' => 'App\Http\Controllers\ProjectController@addNewUser']);
Route::get('editUser/{id}', ['as' => 'user.editUser', 'uses' => 'App\Http\Controllers\ProjectController@editUser']);
Route::post('updateUser',['as' => 'user.updateUser', 'uses' => 'App\Http\Controllers\ProjectController@updateUser']);
Route::get('statusUser/{id}/{status}',['as' => 'user.statusUser', 'uses' => 'App\Http\Controllers\ProjectController@statusUser']);




// Start Info1 for Home  (INFO HOME)-------------------------------------------------------------->

Route::get('editInfos1', ['as' => 'infos1.editInfos1', 'uses' => 'App\Http\Controllers\Infos1Controller@index']);
Route::post('updateInfos1', ['as' => 'infos1.updateInfos1', 'uses' => 'App\Http\Controllers\Infos1Controller@update']); 

// Start Info-------------------------------------------------------------->

Route::get('editInfos', ['as' => 'infos.editInfos', 'uses' => 'App\Http\Controllers\InfosController@index']);
Route::post('updateInfos', ['as' => 'infos.updateInfos', 'uses' => 'App\Http\Controllers\InfosController@update']);



Route::get('editServicesIntro', ['as' => 'servicesintro.editServicesIntro', 'uses' => 'App\Http\Controllers\ServicesIntroController@index']);
Route::post('updateServicesIntro', ['as' => 'servicesintro.updateServicesIntro', 'uses' => 'App\Http\Controllers\ServicesIntroController@update']);







//Start Story banner Route -------------------------------------------------------------------->

	Route::get('storyListBanner', ['as' => 'story.listBanner', 'uses' => 'App\Http\Controllers\OurstoryController@listBanner']);
    //hay lal view
	Route::get('storyaddBanner', ['as' => 'story.addBanner', 'uses' => 'App\Http\Controllers\OurstoryController@addBanner']);
	//hay lal action
	Route::post('storyaddNewBanner',['as' => 'story.addNewStory', 'uses' => 'App\Http\Controllers\OurstoryController@addNewBanner']);

	Route::get('storyeditBanner/{id}', ['as' => 'story.editStory', 'uses' => 'App\Http\Controllers\OurstoryController@editBanner']);
	Route::post('storyupdateBanner',['as' => 'story.updateStory', 'uses' => 'App\Http\Controllers\OurstoryController@updateBanner']);
	Route::get('storydeleteBanner/{id}',['as' => 'story.deleteStory', 'uses' => 'App\Http\Controllers\OurstoryController@deleteBanner']);
	Route::get('storystatusBanner/{id}/{status}',['as' => 'story.statusBanner', 'uses' => 'App\Http\Controllers\OurstoryController@statusBanner']);














//Start Story Who We Are Route -------------------------------------------------------------------->

Route::get('ListWa', ['as' => 'wa.list', 'uses' => 'App\Http\Controllers\OurstoryController@listWa']);
//hay lal view
Route::get('AddWa', ['as' => 'wa.add', 'uses' => 'App\Http\Controllers\OurstoryController@addWa']);
//hay lal action
Route::post('AddNewWa',['as' => 'wa.addNew', 'uses' => 'App\Http\Controllers\OurstoryController@addNewWa']);

Route::get('waeditWa/{id}', ['as' => 'wa.editWa', 'uses' => 'App\Http\Controllers\OurstoryController@editWa']);
Route::post('waupdateWa',['as' => 'wa.updateWa', 'uses' => 'App\Http\Controllers\OurstoryController@updateWa']);
Route::get('wadeleteWa/{id}',['as' => 'story.deleteStory', 'uses' => 'App\Http\Controllers\OurstoryController@deleteWa']);
Route::get('storystatuswa/{id}/{status}',['as' => 'story.statuswa', 'uses' => 'App\Http\Controllers\OurstoryController@statuswa']);
















//Start Story What Makes Route -------------------------------------------------------------------->

Route::get('ListWm', ['as' => 'wm.list', 'uses' => 'App\Http\Controllers\OurstoryController@listWm']);
//hay lal view
Route::get('AddWm', ['as' => 'wm.add', 'uses' => 'App\Http\Controllers\OurstoryController@addWm']);
//hay lal action
Route::post('AddNewWm',['as' => 'wm.addNew', 'uses' => 'App\Http\Controllers\OurstoryController@addNewWm']);

Route::get('wmeditWm/{id}', ['as' => 'wm.editwm', 'uses' => 'App\Http\Controllers\OurstoryController@editWm']);
Route::post('wmupdateWm',['as' => 'wm.updatewm', 'uses' => 'App\Http\Controllers\OurstoryController@updateWm']);
Route::get('wmdeleteWm/{id}',['as' => 'story.deleteStory', 'uses' => 'App\Http\Controllers\OurstoryController@deleteWm']);
Route::get('storystatuswm/{id}/{status}',['as' => 'story.statuswm', 'uses' => 'App\Http\Controllers\OurstoryController@statuswm']);






});




// Start Media Route-------------------------------------------------------------->

// Route::get('listAlbum', ['as' => 'album.listAlbum', 'uses' => 'App\Http\Controllers\AlbumController@listAlbum']);
// Route::get('addAlbum', ['as' => 'album.addAlbum', 'uses' => 'App\Http\Controllers\AlbumController@addAlbum']);
// Route::post('addNewAlbum',['as' => 'album.addNewAlbum', 'uses' => 'App\Http\Controllers\AlbumController@addNewAlbum']);
// Route::get('editAlbum/{id}', ['as' => 'album.editAlbum', 'uses' => 'App\Http\Controllers\AlbumController@editAlbum']);
// Route::post('updateAlbum',['as' => 'album.updateAlbum', 'uses' => 'App\Http\Controllers\AlbumController@updateAlbum']);
// Route::get('deleteAlbum/{id}',['as' => 'album.deleteAlbum', 'uses' => 'App\Http\Controllers\AlbumController@deleteAlbum']);
// Route::get('addImg/{id}', ['as' => 'album.addImg', 'uses' => 'App\Http\Controllers\AlbumController@addImg']);
// Route::post('uploadImg',['as' => 'album.uploadImg', 'uses' => 'App\Http\Controllers\AlbumController@uploadImg']);
// Route::get('fetchImg', ['as' => 'album.fetchImg', 'uses' => 'App\Http\Controllers\AlbumController@fetchImg']);
// Route::post('addImg/deleteImg', ['as' => 'album.deleteImg', 'uses' => 'App\Http\Controllers\AlbumController@deleteImg']);
// Route::post('addImg/sortImages', ['as' => 'album.sortImages', 'uses' => 'App\Http\Controllers\AlbumController@sortImages']);
// Route::get('statusAlbum/{id}/{status}',['as' => 'album.statusAlbum', 'uses' => 'App\Http\Controllers\AlbumController@statusAlbum']);


});