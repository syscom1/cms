<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Exception;
use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Models\Banner;
use App\Models\Ourhome;
use App\Models\Ourimg;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BannerController extends Controller
{

public function listBanner()
{

    $listBanner = Banner::orderBy('banner_id', 'desc')->get();

    return view('banner.listBanner')->with('listBanner', $listBanner);
}



public function addBanner(Request $request)
{
    $listBanner = Banner::orderBy('banner_id','desc')->get();

    return view('banner.addBanner')->with('listBanner',$listBanner);
}

public function addNewBanner(Request $request)
{


    $bannerAdd=new Banner();
    $bannerAdd->title=$request->input('title');
    $bannerAdd->caption=$request->input('caption');
         
        $image = $request->banner_img;
        $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
        $image->move('uploaded/', $fileName);
        $uploadFile = 'uploaded/' . $fileName;
    $bannerAdd->banner_img=$uploadFile;
    $bannerAdd->banner_url=$request->input('link');
    $bannerAdd->save();
    $message = "Record has been added successfully";

    //return redirect()->route('banner.addBanner')->withErrors($message);
    return redirect()->route('banner.listBanner')->withErrors($message);

}



public function editBanner(Request $request,$id)
{
    $editBanner = Banner::find($id);
 

    return view('banner.editBanner')->with('editBanner', $editBanner);
                                    
 
}


public function updateBanner(Request $request)
{
      


    $banner_update =Banner::find($request->banner_id);
    $banner_update->title=$request->input('title');
    $banner_update->caption=$request->input('caption');
    if($request->hasFile('banner_img')){
        unlink($banner_update->banner_img);
        $image = $request->banner_img;
        $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
        $image->move('uploaded/', $fileName);
        $uploadFile = 'uploaded/' . $fileName;
        $banner_update->banner_img=$uploadFile;
    }
    $banner_update->banner_url=$request->input('link');
    $banner_update->update();
    $message = "Record has been updated successfully";
   return redirect()->route('banner.listBanner')->withErrors($message);

}
 
public function statusBanner(Request $request,$id,$status)
    {
        $banner=Banner::find($id);
        $banner->banner_status=$status;
        $banner->update();
        $message = "Status has been changed successfully";
        return redirect()->back()->withErrors($message);
    }


    
public function deleteBanner(Request $request,$id)
{
    $banner=Banner::find($id);
    unlink($banner->banner_img);
    $banner->delete();
    $message = "Record has been deleted successfully";
    return redirect()->back()->withErrors($message);
}


public function listOur(){
    $our=Ourhome::all();
    return view('ourhome.listOur',compact('our'));
}

public function addOurhome(Request $request){
    return view('ourhome.addOur');
}

public function addnewourhome(Request $request){
    $our=new Ourhome();
    $our->page1_title=$request->input('our_title');
    $our->page1_text=$request->input('our_caption');
    $our->save();
    return redirect()->route('banner.listourwork')->with('err','Record has been saved successfully');
}

public function editOurHome($id){
    $our=Ourhome::find($id);
    return view('ourhome.editOur',compact('our'));
}
public function updateOurHome(Request $request){
    $our=Ourhome::find($request->input('id'));
    $our->page1_title=$request->input('our_title');
    $our->page1_text=$request->input('our_caption');
    $our->update();
    return redirect()->route('banner.listourwork')->with('err','Record has been updated successfully');
}
public function deleteOurHome($id){
    $our=Ourhome::find($id);
    $our->delete();
    return redirect()->route('banner.listourwork')->with('err','Record has been deleted successfully');
}



public function addImg(Request $request)
{
    $result = array();
      $result['page1_image']=  Ourimg::all();
      
       foreach ( $result['page1_image'] as $i => $a) {
       
           $a->size = File::size($a->page1_image);
             $a->name = File::name($a->page1_image);
       }

    return view('ourhome.addImg')->with('result', $result);

}


public function uploadImg(Request $request)
    {
       
        // $our_id=$request->id;
        $res= [];
        // $album_im ;
    
        	if($request->hasFile('file')){
 
            	$cars_pictures = $request->file;
            foreach ($cars_pictures as $i => $a) {
            $image = $a;
			$fileName = time().rand(1000,50000).'.jpg';
			$image->move('uploaded/', $fileName);
			$uploadImage = 'uploaded/'.$fileName; 
			// $car_im =Ourimg::create([
			//    	'page1_image'=>$uploadImage,
			// 		]);	
            $img=new Ourimg();
            $img->page1_image=$uploadImage;
            $img->save();
            }
        }
   $images= Ourimg::get();
   foreach ($images as $i => $a) {
           $a->size = File::size($a->page1_image);
             $a->name = File::name($a->page1_image);
       }
       return response()->json(['success'=>$images,'res'=>$res]);

    }


     
function deleteImg(Request $request)
    {
        
       $imgs= Ourimg::where('id',$request->id)->get();
          foreach ( $imgs as $i => $a) {
      
        $path=public_path().'/'.$a->page1_image;
       if (file_exists($path)) {
            unlink($path);
        }
          }
        Ourimg::where('id',$request->id)->delete();
            

        // $filename =  $request->get('filename');
        // ImageUpload::where('filename',$filename)->delete();
        // $path=public_path().'/images/'.$filename;
        // if (file_exists($path)) {
        //     unlink($path);
        // }
         return response()->json(['success'=>'$path']);
        
    }

public function sortImages(Request $request){
	        $im = json_decode($request->data,true);
  foreach ($im['imgQ']  as $i => $a) {
    Ourimg::where('ourworkimages_id',$a['id'])->update([
         'ourworkimages_sort'=>$a['sort'] +1
         ]);
}
             return response()->json(['success'=>$im['imgQ']  ]);


	}

}