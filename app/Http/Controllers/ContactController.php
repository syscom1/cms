<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Models\Contact;
use Illuminate\Support\Facades\Hash;
use File;
use App\Models\ContactUs;
use Mail;

class ContactController extends Controller
{
    public function listContact()
    {

        $listContact = Contact::all();

    return view('contact.listContact')->with('listContact', $listContact);

        

    }

    public function addContact(Request $request)
    {
        $s=array('s' => '');
        return view('contact.addContact')->with($s);
    }


    public function addNewContact(Request $request)
    {

try{
    $contactAdd=new Contact();
    $contactAdd->contact_address=$request->input('contact_address');
    $contactAdd->contact_phone=$request->input('contact_phone');
    $contactAdd->contact_email=$request->input('contact_email');
    $contactAdd->contact_facebook=$request->input('contact_facebook');
    $contactAdd->contact_instagram=$request->input('contact_instagram');
    $contactAdd->contact_twitter=$request->input('contact_twitter');
    $contactAdd->fax=$request->input('fax');
    if($request->hasFile('image')){
        $image = $request->image;
        $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
        $image->move('uploaded/', $fileName);
        $uploadFile = 'uploaded/' . $fileName;
        $contactAdd->image=$uploadFile;
    }
    $contactAdd->save();
    $s=array('s' => '1');
    $message = "Record has been added successfully";

    return redirect()->route('contact.listContact')->withErrors($message);
                       

}
catch(Exception $e)
{
    $s=array('s' => '0');

    $message = "Cannot Duplicate Contact, This Name Exists !!";

    return view('contact.addContact')->with($s)->withErrors($message);

}
        
    }


    public function editContact(Request $request,$id)
    {
        $editContact = Contact::find($id);

        return view('contact.editContact')->with('editContact',$editContact);
     
    }

    public function updateContact(Request $request)
    {

    $contactAdd=Contact::find($request->input('contact_id'));
    $contactAdd->contact_address=$request->input('contact_address');
    $contactAdd->contact_phone=$request->input('contact_phone');
    $contactAdd->contact_email=$request->input('contact_email');
    $contactAdd->contact_facebook=$request->input('contact_facebook');
    $contactAdd->contact_instagram=$request->input('contact_instagram');
    $contactAdd->contact_twitter=$request->input('contact_twitter');
    $contactAdd->fax=$request->input('fax');
    if($request->hasFile('image')){
        unlink($contactAdd->image);
        $image = $request->image;
        $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
        $image->move('uploaded/', $fileName);
        $uploadFile = 'uploaded/' . $fileName;
        $contactAdd->image=$uploadFile;
    }
    $contactAdd->update();
        $message = "Record has been updated successfully";
        return redirect()->route('contact.listContact')->withErrors($message);

    }


   
    public function statusContact(Request $request,$id,$status)
    {
        $contact=Contact::find($id);
        $contact->contact_status=$status;
        $contact->update();
        $message = "Status has been changed successfully";
        return redirect()->back()->withErrors($message);
    }

    
    public function deleteContact(Request $request,$id)
{
    $contact=Contact::find($id);
    $contact->delete();
    $message = "Record has been deleted successfully";
    return redirect()->back()->withErrors($message);
}


}
