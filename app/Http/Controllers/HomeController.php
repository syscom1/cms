<?php
namespace App\Http\Controllers;

use DB;
use Exception;
use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Models\Users;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
      
        $res = array();


    // $res['p'] =  DB::table('product_tbl')
    //                     ->where('product_status','=','1')
    //                     ->count();

 $res['u'] =  Users::where('type','=','2')->count();

 
                        
 
                                               
                        
                        


//  $listProduct = DB::table('product_tbl')
                   
//                         ->orderBy('product_id', 'desc')
//                         ->take(10)->get();        
                        
//           foreach(  $listProduct as $k=>$a)
//     {
//         $a->product_sub  = implode(" , ",DB::table('subcategory_product_tbl')
//             ->leftJoin('subcategory_tbl','subcategory_tbl.subcategory_id','subcategory_product_tbl.subcategory_id')
//             ->where('subcategory_product_tbl.product_id', $a->product_id )
//             ->pluck('subcategory_tbl.subcategory_name')->toArray());
//     }
    
    
        $listUsers = Users::where('type','=','2')->orderBy('id', 'desc')->take(10)->get();        

        
        return view('dashboard')->with('listUsers', $listUsers)->with('res',$res);
    }
}
