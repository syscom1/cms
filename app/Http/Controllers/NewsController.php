<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function listNews(){
        $news=News::all();
        return view('news.listNews',compact('news'));
    }
    public function addNews(){
        $news=News::all();
        return view('news.addNews',compact('news'));
    }
    public function addnewNews(Request $request){
        $title=$request->input('news_title');
        $caption=$request->input('news_caption');
        $news=new News();
        $news->news_title=$title;
        $news->news_caption=$caption;     

        if ($request->hasFile('news_image')) {
         
        $image = $request->news_image;
        $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
        $image->move('uploaded/', $fileName);
        $uploadFile = 'uploaded/' . $fileName;
        }
        else
        {
             $uploadFile=null;
        }
        $news->news_image=$uploadFile;
        $news->save();
        $news=News::all();
        return redirect()->route('news.listNews')->with('err','Record has been saved successfully.');
    }

       public function editNews($id){
           $news=News::find($id);
            return  view('news.editNews',compact('news'));
       }

       public function updateNews(Request $request){
        $id=$request->input('id');

        $news=News::find($id);
 
        if ($request->hasFile('news_image')) {
            unlink($news->news_image);
            $image = $request->news_image;
            $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
            $image->move('uploaded/', $fileName);
            $uploadFile = 'uploaded/' . $fileName;
            }
            else
            {
            $uploadFile=  $news->news_image;
            }
 
        $news->news_title=$request->input('news_title');
        $news->news_caption=$request->input('news_caption');
        $news->news_image=$uploadFile;
        $news->update();
        return redirect()->route('news.listNews')->with('err','Record has been updated successfully');
    }
    public function deleteNews($id){
        $news=News::find($id);
        $news->delete();
        return back()->with('err','Record has been deleted successfully');
    }
}
