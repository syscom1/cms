<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Models\Users;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use File;
class ClientController extends Controller
{

public function listUserAddress(Request $request){
    
                  $listUserData = Users::where('id', $request->id)->first();


    
    $listUserAddress = DB::table('address_user_tbl')
                        ->leftJoin('address_tbl', 'address_tbl.address_id', 'address_user_tbl.address_id')
                        ->leftJoin('users', 'users.id', 'address_user_tbl.user_id')
                        ->leftJoin('city_tbl', 'city_tbl.city_id','address_tbl.address_city')
                        ->orderBy('address_user_tbl.address_user_id', 'desc')
                        ->where('users.id', $request->id)
                        ->get();    
    
    return view('user.listUserAddress')->with('listUserAddress', $listUserAddress)->with('listUserData',$listUserData);

}


public function listUserOrders(Request $request){
    
    
            $listUserOrders = DB::table('users')
                            ->leftJoin('order_tbl', 'order_tbl.user_id', 'users.id')
                                                        ->where('order_status', 1)

                            ->where('users.id', $request->id)
                            ->get();
            
return view('user.listUserOrders')->with('listUserOrders', $listUserOrders);
}



public function listClient(Request $request)
{        $details = $request->id;

    
    $listClient = DB::table('users')
                ->where('type', '=', '2')
                ->when($details, function ($query, $details) {
                return $query->where('id', $details);
                             })  
                 ->orderBy('id', 'desc')
                 ->get();
                 
             
                        
                 return view('user.listClient')->with('listClient', $listClient);
}
 
 
}