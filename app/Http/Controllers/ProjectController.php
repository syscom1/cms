<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Models\Address_user;
use App\Models\Users;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use File;
class ProjectController extends Controller
{

 
  

// User FUNCTIONS --------------------------------------------------------------->


 
public function listUser(Request $request)
{
    $type = $request->type;
    $search =$request->search;

    $listUser = Users::orderBy('id', 'desc')->where('type', '=', '1')->get();

    return view('user.listUser')->with('listUser', $listUser);
}


public function addUser(){
    return view('user.addUser');

}


public function addNewUser(Request $request){

    // $userid = DB::table('users')->insertGetId([
    //     'name' => $request->name,
    //     'email' => $request->email,
    //     'type' => $request->typeS1,
    //     'phone' => $request->phone,
    //     'password' => ($request->typeS1 == "1") ? Hash::make($request->password) : $request->password


    // ]);
    $user=new Users();
    $user->name= $request->input('name');
    $user->email=$request->input('email');
    $user->type=$request->input('typeS1');
    $user->phone=$request->input('phone');
    $user->password=($request->typeS1 == "1") ? Hash::make($request->password) : $request->password;
    $user->save();
        $message = "Record has been added successfully";

        return redirect()->route('user.addUser')->withErrors($message);
}

public function statusUser(Request $request,$id,$status)
    {
        $user=Users::find($id);
        $user->status=$status;
        $user->update();
        $message = "Status has been changed successfully";
        return redirect()->back()->withErrors($message);
    }
public function editUser(Request $request,$id)
    {
        $editUser =Users::find($id);
        $addressUser = [];
// if( Users::where('user_id', $id)->exists())
// {
// $addressUser= Address_user::where('user_id', $request->id)->first();
// }
    return view('user.editUser')->with('editUser', $editUser);
 
       }
   
    public function updateUser(Request $request)
{
    $id=$request->input('id');
    $user=Users::find($id);
    $user->name= $request->input('name');
    $user->email=$request->input('email');
    $user->type="1";
    $user->phone=$request->input('phone');
    $user->password=($request->typeS1 == "1") ? Hash::make($request->password) : $request->password;
    $user->update();
                        
   
    $message = "Record has been updated successfully";

    return redirect()->route('user.listUser')->withErrors($message);
}

// Report FUNCTIONS --------------------------------------------------------------->
    public function listReport(Request $request)
{
    
    
        $category_id = $request->category_id;
        $subcategory_id = $request->subcategory_id;
       

        $reportList = DB::table('product_tbl')
            ->leftJoin('subcategory_product_tbl', 'subcategory_product_tbl.product_id', 'product_tbl.product_id')
            ->leftJoin('subcategory_tbl', 'subcategory_tbl.subcategory_id', 'subcategory_product_tbl.subcategory_id')
            ->leftJoin('category_tbl', 'category_tbl.category_id', 'subcategory_tbl.category_id')

           // ->whereRaw('status_tbl.status_id IN (SELECT MAX(status_tbl.status_id) FROM status_tbl GROUP BY status_tbl.order_id)')
            //->where('status_tbl.status_type', '<>', 'P')

            ->when($category_id, function ($query, $category_id) {
                return $query->where('category_tbl.category_id', $category_id);
            })
            ->when($subcategory_id, function ($query, $subcategory_id) {
                return $query->where('subcategory_tbl.subcategory_id', $subcategory_id);
            })
            // ->where('status_tbl.status_type','<>','P')
            ->orderBy('product_tbl.product_id', 'desc')
            ->get();

        $reportCat = DB::table('category_tbl')
            ->orderBy('category_id', 'desc')
            ->get();

        $reportSubcat = DB::table('subcategory_tbl')
            ->orderBy('subcategory_id', 'desc')
            ->get();

       

        return view('report.listReport')->with('reportList', $reportList)
            ->with('reportCat', $reportCat)
            ->with('reportSubcat', []);
    
    
    
}

// Report2 FUNCTIONS --------------------------------------------------------------->
    public function listReport2(Request $request)
{
    
    
        // $category_id = $request->category_id;
        // $subcategory_id = $request->subcategory_id;
       

        $reportList = DB::table('product_tbl')
            ->leftJoin('subcategory_product_tbl', 'subcategory_product_tbl.product_id', 'product_tbl.product_id')
            ->leftJoin('subcategory_tbl', 'subcategory_tbl.subcategory_id', 'subcategory_product_tbl.subcategory_id')
            ->leftJoin('category_tbl', 'category_tbl.category_id', 'subcategory_tbl.category_id')
            ->where('product_tbl.product_quantity','0')    
           // ->whereRaw('status_tbl.status_id IN (SELECT MAX(status_tbl.status_id) FROM status_tbl GROUP BY status_tbl.order_id)')
            //->where('status_tbl.status_type', '<>', 'P')

            // ->when($category_id, function ($query, $category_id) {
            //     return $query->where('category_tbl.category_id', $category_id);
            // })
            // ->when($subcategory_id, function ($query, $subcategory_id) {
            //     return $query->where('subcategory_tbl.subcategory_id', $subcategory_id);
            // })
            // ->where('status_tbl.status_type','<>','P')
            ->orderBy('product_tbl.product_id', 'desc')
            ->get();

        $reportCat = DB::table('category_tbl')
            ->orderBy('category_id', 'desc')
            ->get();

        $reportSubcat = DB::table('subcategory_tbl')
            ->orderBy('subcategory_id', 'desc')
            ->get();

       

return view('report2.listReport2')->with('reportList', $reportList);
        //     ->with('reportCat', $reportCat)
        //     ->with('reportSubcat', $reportSubcat);
    
    
    
}



public function getSubcategory(Request $request)
  {

 // Fetch Employees by Departmentid ['data']
 $getSubcategory= DB::table('subcategory_tbl')
 ->where('category_id',$request->id)
 ->get();

return $getSubcategory;
  }


}