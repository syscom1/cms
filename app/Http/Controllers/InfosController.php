<?php

namespace App\Http\Controllers;

use App\Models\Info;
use DB;
use Illuminate\Http\Request;
use File;
use Collection;
class InfosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
 
        $editInfo = Info::first();

        return view('infos.info')->with('editInfo',$editInfo);
    }
    
    
 
      public function update(Request $request,$id)
    {
        //dd($request->files);

        
        try{

 
        if ($request->hasFile('info_image1')) {
            // $size= $request->file('banner_img')->getClientSize();
       //   dd('image1');
    
             $image1 = $request->info_image1;
             $fileName1 = time() . '.' . $image1->getClientOriginalExtension();
             $image1->move('resources/assets/images/info_images/', $fileName1);
             $uploadFile1 = 'resources/assets/images/info_images/' . $fileName1;
         } else {

         //   dd('else1');
             //$size1='';
             $uploadFile1 = Info::where('info_id','=',$request->id)->first()->info_image1;
         } 
      
        $storiesAdd=Info::find($id);
        $storiesAdd->info_about_us=$request->input('info_about_us');
        $storiesAdd->info_about_us_fr=$request->input('info_about_us_fr');
        $storiesAdd->info_image1=$uploadFile1;
        $storiesAdd->update();
//  ma32oule min wara hayda ino fo2 mish nafes lita7et?
    $message = "Saved !";

        return redirect()->back()->withErrors($message);
                                           

  }
catch(Exception $e)
{
 

    $message = "Error!";

        return redirect()->back()->withErrors($message);

}
    }
    
}