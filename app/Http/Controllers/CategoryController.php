<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Models\Category;
use App\Models\Catimage;
use App\Models\Menu;
use App\Models\Ourimg;
use App\Models\Subcategory;
use App\Models\Subimage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
class CategoryController extends Controller
{

// CATEGORY FUNCTIONS --------------------------------------------------------------->
    public function listCategory(Request $request)
    {
        $listCategory=Category::all();

        return view('category.listCategory')->with('listCategory', $listCategory);
    }



    public function addCategory(Request $request)
    {
        $s=array('s' => '');
        $listMenu = Menu::orderBy('menu_id','asc')->get();

        return view('category.addCategory')->with($s)
                                             ->with('listMenu', $listMenu);
    }



    public function addNewCategory(Request $request)
    {
        $category=new Category();
        $category->name=$request->input('banner_title');
        $category->banner_link=$request->input('banner_link');
        if($request->hasFile('banner_img')){
        $image = $request->banner_img;
        $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
        $image->move('uploaded/', $fileName);
        $uploadFile = 'uploaded/' . $fileName;
        $category->banner_photo=$uploadFile;
        }
        $category->cover_title=$request->input('cover_title');
        $category->cover_text=$request->input('cover_text');
        if($request->hasFile('cover_img')){
            $image1 = $request->cover_img;
            $fileName1 = time().rand(1000,50000) . '.' . $image1->getClientOriginalExtension();
            $image1->move('uploaded/', $fileName1);
            $uploadFile1 = 'uploaded/' . $fileName1;
            $category->cover_photo=$uploadFile1;
            }
        $category->save();
        $message = "Record has been added successfully";
        return redirect()->route('category.listCategory')->withErrors($message);
 
    }



    public function editCategory(Request $request)
    {
    $editCategory = Category::where('category_id', $request->id)
    ->first();

   

    $listMenu= Menu::orderBy('menu_id','asc')->get();

        return view('category.editCategory')->with('editCategory', $editCategory)
                                            ->with('listMenu', $listMenu);
     
    }


    public function updateCategory(Request $request)
    {
        $category=Category::find($request->input('category_id'));
        $category->name=$request->input('banner_title');
        $category->banner_link=$request->input('banner_link');
        if($request->hasFile('banner_img')){
        $image = $request->banner_img;
        $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
        $image->move('uploaded/', $fileName);
        $uploadFile = 'uploaded/' . $fileName;
        $category->banner_photo=$uploadFile;
        }
        $category->cover_title=$request->input('cover_title');
        $category->cover_text=$request->input('cover_text');
        if($request->hasFile('cover_img')){
            $image1 = $request->cover_img;
            $fileName1 = time().rand(1000,50000) . '.' . $image1->getClientOriginalExtension();
            $image1->move('uploaded/', $fileName1);
            $uploadFile1 = 'uploaded/' . $fileName1;
            $category->cover_photo=$uploadFile1;
            }
        $category->save();
         $message = "Record has been updated successfully";
       return redirect()->route('category.listCategory')->withErrors($message);

    }

    public function statusCategory(Request $request,$id,$status)
    {
        $category=Category::find($id);
        $category->category_status=$status;
        $category->update();
        $message = "Status has been changed successfully";
        return redirect()->back()->withErrors($message);
    }

    public function deleteCategory(Request $request,$id)
    {
        $category=Category::find($id);
        $category->delete();
        $message = "Record has been deleted successfully";
        return redirect()->back()->withErrors($message);
    }
    

    public function listSubcategory(Request $request)
    {
        $listSubcategory=Subcategory::query()->join('category_details','category_details.category_id','=','subcategory_details.category_id')
        ->select('subcategory_details.id','category_details.name','subcategory_details.banner_title','subcategory_details.banner_photo','subcategory_details.banner_link','subcategory_details.cover_title','subcategory_details.cover_text','subcategory_details.cover_photo','subcategory_details.category_status')->get();
        // ->get('subcategory_details.banner_title');
      return view('Subcategory.listSubcategory')->with('listSubcategory', $listSubcategory);
    }



    public function addSubcategory(Request $request)
    {
        $category=Category::all();
        $s=array('s' => '');
        $listMenu = Menu::orderBy('menu_id','asc')->get();

        return view('Subcategory.addSubcategory',compact('category'))->with($s)
                                             ->with('listMenu', $listMenu);
    }



    public function addNewSubcategory(Request $request)
    {
        $Subcategory=new Subcategory();
        $Subcategory->category_id=$request->input('cat_name');
        $Subcategory->banner_title=$request->input('banner_title');
        $Subcategory->banner_link=$request->input('banner_link');
        if($request->hasFile('banner_img')){
        $image = $request->banner_img;
        $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
        $image->move('uploaded/', $fileName);
        $uploadFile = 'uploaded/' . $fileName;
        $Subcategory->banner_photo=$uploadFile;
        }
        $Subcategory->cover_title=$request->input('cover_title');
        $Subcategory->cover_text=$request->input('cover_text');
        if($request->hasFile('cover_img')){
            $image1 = $request->cover_img;
            $fileName1 = time().rand(1000,50000) . '.' . $image1->getClientOriginalExtension();
            $image1->move('uploaded/', $fileName1);
            $uploadFile1 = 'uploaded/' . $fileName1;
            $Subcategory->cover_photo=$uploadFile1;
            }
        $Subcategory->save();
        $message = "Record has been added successfully";
        return redirect()->route('Subcategory.listSubcategory')->withErrors($message);

    }



    public function editSubcategory(Request $request,$id)
    {
    $category=Category::all();
    $editSubcategory = Subcategory::find($id);
    $listMenu= Menu::orderBy('menu_id','asc')->get();

        return view('Subcategory.editSubcategory',compact('category'))->with('editSubcategory', $editSubcategory)
                                            ->with('listMenu', $listMenu);
     
    }


    public function updateSubcategory(Request $request)
    {
        $Subcategory=Subcategory::find($request->input('id'));
        $Subcategory->category_id=$request->input('cat_name');
        $Subcategory->banner_title=$request->input('banner_title');
        $Subcategory->banner_link=$request->input('banner_link');
        if($request->hasFile('banner_img')){
        $image = $request->banner_img;
        $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
        $image->move('uploaded/', $fileName);
        $uploadFile = 'uploaded/' . $fileName;
        $Subcategory->banner_photo=$uploadFile;
        }
        $Subcategory->cover_title=$request->input('cover_title');
        $Subcategory->cover_text=$request->input('cover_text');
        if($request->hasFile('cover_img')){
            $image1 = $request->cover_img;
            $fileName1 = time().rand(1000,50000) . '.' . $image1->getClientOriginalExtension();
            $image1->move('uploaded/', $fileName1);
            $uploadFile1 = 'uploaded/' . $fileName1;
            $Subcategory->cover_photo=$uploadFile1;
            }
        $Subcategory->save();
         $message = "Record has been updated successfully";
       return redirect()->route('Subcategory.listSubcategory')->withErrors($message);

    }

    public function statusSubcategory(Request $request,$id,$status)
    {
        $Subcategory=Subcategory::find($id);
        $Subcategory->category_status=$status;
        $Subcategory->update();
        $message = "Status has been changed successfully";
        return redirect()->back()->withErrors($message);
    }

    public function deleteSubcategory(Request $request,$id)
    {
        $Subcategory=Subcategory::find($id);
        $Subcategory->delete();
        $message = "Record has been deleted successfully";
        return redirect()->back()->withErrors($message);
    }

    public function addImg(Request $request,$id)
{
    $result = array();
    session(['catid'=>$id]);
      $result['image']=  Catimage::where('category_id',$id)->get();
      
       foreach ( $result['image'] as $i => $a) {
       
           $a->size = File::size($a->image);
             $a->name = File::name($a->image);
       }

    return view('category.addImg')->with('result', $result);

}


public function uploadImg(Request $request)
    {
    
        $res= [];
        // $album_im ;
    
        	if($request->hasFile('file')){
 
            	$cars_pictures = $request->file;
            foreach ($cars_pictures as $i => $a) {
            $image = $a;
			$fileName = time().rand(1000,50000).'.jpg';
			$image->move('uploaded/', $fileName);
			$uploadImage = 'uploaded/'.$fileName; 
			// $car_im =Ourimg::create([
			//    	'page1_image'=>$uploadImage,
			// 		]);	
            $img=new Catimage();
            $img->image=$uploadImage;
            $img->category_id=session('catid');
            $img->save();
            }
        }
   $images= Catimage::get();
   foreach ($images as $i => $a) {
           $a->size = File::size($a->image);
             $a->name = File::name($a->image);
       }
       return response()->json(['success'=>$images,'res'=>$res]);

    }


     
function deleteImg(Request $request)
    {
        
       $imgs= Catimage::where('id',$request->id)->get();
          foreach ( $imgs as $i => $a) {
      
        $path=public_path().'/'.$a->image;
       if (file_exists($path)) {
            unlink($path);
        }
          }
        Catimage::where('id',$request->id)->delete();
            

        // $filename =  $request->get('filename');
        // ImageUpload::where('filename',$filename)->delete();
        // $path=public_path().'/images/'.$filename;
        // if (file_exists($path)) {
        //     unlink($path);
        // }
         return response()->json(['success'=>'$path']);
        
    }

// public function sortImages(Request $request){
// 	        $im = json_decode($request->data,true);
//   foreach ($im['imgQ']  as $i => $a) {
//     Catimage::where('ourworkimages_id',$a['id'])->update([
//          'ourworkimages_sort'=>$a['sort'] +1
//          ]);
// }
//              return response()->json(['success'=>$im['imgQ']  ]);


// 	}



    public function addImgSub(Request $request,$id)
    {
        $result = array();
        session(['subid'=>$id]);
          $result['image']=  Subimage::where('subcategory_id',$id)->get();
          
           foreach ( $result['image'] as $i => $a) {
           
               $a->size = File::size($a->image);
                 $a->name = File::name($a->image);
           }
    
        return view('subcategory.addImg')->with('result', $result);
    
    }
    
    
    public function uploadImgSub(Request $request)
        {
        

            $res= [];
            // $album_im ;
        
                if($request->hasFile('file')){
     
                    $cars_pictures = $request->file;
                foreach ($cars_pictures as $i => $a) {
                $image = $a;
                $fileName = time().rand(1000,50000).'.jpg';
                $image->move('uploaded/', $fileName);
                $uploadImage = 'uploaded/'.$fileName; 
                // $car_im =Ourimg::create([
                //    	'page1_image'=>$uploadImage,
                // 		]);	
                $img=new Subimage();
                $img->image=$uploadImage;
                $img->subcategory_id=session('subid');
                $img->save();
                }
            }
       $images= Subimage::get();
       foreach ($images as $i => $a) {
               $a->size = File::size($a->image);
                 $a->name = File::name($a->image);
           }
           return response()->json(['success'=>$images,'res'=>$res]);
    
        }
    
    
         
    function deleteImgSub(Request $request)
        {
            
           $imgs= Subimage::where('id',$request->id)->get();
              foreach ( $imgs as $i => $a) {
          
            $path=public_path().'/'.$a->image;
           if (file_exists($path)) {
                unlink($path);
            }
              }
              Subimage::where('id',$request->id)->delete();
                
    
            // $filename =  $request->get('filename');
            // ImageUpload::where('filename',$filename)->delete();
            // $path=public_path().'/images/'.$filename;
            // if (file_exists($path)) {
            //     unlink($path);
            // }
             return response()->json(['success'=>'$path']);
            
        }
    
    // public function sortImagesSub(Request $request){
    //             $im = json_decode($request->data,true);
    //   foreach ($im['imgQ']  as $i => $a) {
    //     Subimage::where('id',$a['id'])->update([
    //          'ourworkimages_sort'=>$a['sort'] +1
    //          ]);
    // }
    //              return response()->json(['success'=>$im['imgQ']  ]);
    
    
    //     }
}