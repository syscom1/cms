<?php

namespace App\Http\Controllers;

use App\Models\Infos1;
use DB;
use Illuminate\Http\Request;
use File;
use Collection;

class Infos1Controller extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
 
        $editInfo1=Infos1::first();

        return view('infos1.info1')->with('editInfo1',$editInfo1);
    }
    
    
 
      public function update(Request $request)
    {
        //dd($request->files);

        
        try{

            if ($request->hasFile('info1_image')) {
                // $size= $request->file('banner_img')->getClientSize();
           //   dd('image1');
        
                 $image1 = $request->info1_image;
                 $fileName1 = time() . '.' . $image1->getClientOriginalExtension();
                 $image1->move('resources/assets/images/info1_images/', $fileName1);
                 $uploadFile1 = 'resources/assets/images/info1_images/' . $fileName1;
             } else {
    
             //   dd('else1');
                 //$size1='';
                 $uploadFile1 = DB::table('info1_tbl')->where('info1_id','=',$request->id)->first()->info1_image;
             } 
        
       
    $info1Add = DB::table('info1_tbl')->where('info1_id','=',$request->id)->update([
      
                          'info1_about_us'=>$request->info1_about_us,
                          'info1_about_us_fr'=>$request->info1_about_us_fr,	 
                          'info1_image'=>$uploadFile1

					]);
    $message = "Saved !";

        return redirect()->back()->withErrors($message);
                                           

  }
catch(Exception $e)
{
 

    $message = "Error!";

        return redirect()->back()->withErrors($message);

}
    }
}
