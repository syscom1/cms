<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use File;
use Collection;


class MembersIntroController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
 
        $editMembersIntro = DB::table('memberintro')
 
            ->first();

        return view('membersintro.editMembersIntro')->with('editMembersIntro',$editMembersIntro);
    }
    
    
 
      public function update(Request $request)
    {
        //dd($request->files);

        
        try{


    $editMembersIntro = DB::table('memberintro')->where('memberintro_id','=',$request->id)->update([
      
                          'memberintro_text'=>$request->memberintro_text,
                          'memberintro_text_fr'=>$request->memberintro_text_fr



					]);
    $message = "Saved !";

        return redirect()->back()->withErrors($message);
                                           

  }
catch(Exception $e)
{
 

    $message = "Error!";

        return redirect()->back()->withErrors($message);

}
    }
}
