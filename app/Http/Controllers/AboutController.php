<?php

namespace App\Http\Controllers;

use App\Models\About_banner;
use App\Models\About_info;
use App\Models\About_team;
use Illuminate\Http\Request;
use Laravel\Ui\Presets\React;

class AboutController extends Controller
{
    public function listBanner(){
        $listBanner=About_banner::all();
        return view('about.banner.listBanner',compact('listBanner'));
    }
    public function addBanner(){
        return view('about.banner.addBanner');
    }
    public function addNewBanner(Request $request){
        $banner=new About_banner();
        $banner->banner_title=$request->input('banner_title');
        $banner->banner_caption=$request->input('banner_caption');
        $banner->save();
        return redirect()->route('about.listBanner')->with('err','Record has been saved successfully.');
    }
    public function editBanner($id){
        $banner=About_banner::find($id);
        return view('about.banner.editBanner',compact('banner'));
    }
    public function updateBanner(Request $request){
        $id=$request->input('banner_id');
        $banner=About_banner::find($id);
        $banner->banner_title=$request->input('banner_title');
        $banner->banner_caption=$request->input('banner_caption');
        $banner->update();
        return redirect()->route('about.listBanner')->with('err','Record has been updated successfully.');
    }
    public function deleteBanner($id){
        $banner=About_banner::find($id);
        $banner->delete();
        return redirect()->route('about.listBanner')->with('err','Record has been deleted successfully.');
 
    }
    public function editTeamInfo(){
        $info=About_info::first();
        return view('about.info.info',compact('info')); 
    }
    public function updateTeamInfo(Request $request){
        $info=About_info::first();
        $info->info_caption=$request->input('info_caption');
        $info->update();
        return redirect()->route('about.editTeamInfo')->with('err','Record has been updated successfully.');
    }
    public function listTeam(){
        $team=About_team::all();
        return view('about.team.listTeam',compact('team'));
    }
    public function addTeam(){
        return view('about.team.addTeam');
    }
    public function addNewTeam(Request $request){
        if ($request->hasFile('team_image')) {
         
            $image = $request->team_image;
            $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
            $image->move('uploaded/', $fileName);
            $uploadFile = 'uploaded/' . $fileName;
            }
            else
            {
                 $uploadFile=null;
            }
        $team=new About_team();
        $team->team_name=$request->input('team_name');
        $team->team_caption=$request->input('team_caption');
        $team->team_image=$uploadFile;
        $team->save();
        return redirect()->route('about.listTeam')->with('err','Record has been saved successfully.');
    }
    public function editTeam($id){
         $team=About_team::find($id);
         return view('about.team.editTeam',compact('team'));
    }
    public function updateTeam(Request $request){
        $id=$request->input('id');

        $team=About_team::find($id);
 
        if ($request->hasFile('team_image')) {
            unlink($team->team_image);
            $image = $request->team_image;
            $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
            $image->move('uploaded/', $fileName);
            $uploadFile = 'uploaded/' . $fileName;
            }
            else
            {
            $uploadFile=  $team->team_image;
            }
 
        $team->team_name=$request->input('team_name');
        $team->team_caption=$request->input('team_caption');
        $team->team_image=$uploadFile;
        $team->update();
        return redirect()->route('about.listTeam')->with('err','Record has been updated successfully');
    }
    public function deleteTeam($id){
        $team=About_team::find($id);
        $team->delete();
        return redirect()->route('about.listTeam')->with('err','Record has been deleted successfully');
    }
}
