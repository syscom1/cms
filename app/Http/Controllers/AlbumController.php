<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Exception;
use App\Models\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use File;

class AlbumController extends Controller
{
    public function listAlbum()
{

    $listAlbum = DB::table('album_tbl') ->get();

return view('album.listAlbum')->with('listAlbum', $listAlbum);
}



public function addAlbum(Request $request)
{
    $s=array('s' => '');
    return view('album.addAlbum')->with($s);
}

public function addNewAlbum(Request $request)
{

     if ($request->hasFile('album_image')) {
            // $size= $request->file('banner_img')->getClientSize();
          
    
             $image = $request->album_image;
             $fileName = time() . '.' . $image->getClientOriginalExtension();
             $image->move('resources/assets/images/album_images/', $fileName);
             $uploadFile = 'resources/assets/images/album_images/' . $fileName;
         } else {
             $size='';
             $uploadFile = '';
         }

    $albumAdd = DB::table('album_tbl')->insertGetId([
        'album_id' =>$request->album_id,
        // 'album_title' => $request->album_title,
        'album_position' => $request->album_position,
        'album_image'=>$uploadFile

    ]);
    $message = "Record has been added successfully";

   // return redirect()->route('album.addAlbum')->withErrors($message);
   return redirect()->route('album.listAlbum')->withErrors($message);

}



public function editAlbum(Request $request)
{
    $editAlbum = DB::table('album_tbl')->where('album_id', $request->id)->first();

        return view('album.editAlbum')->with('editAlbum', $editAlbum);
 
}


public function updateAlbum(Request $request)
{
      if ($request->hasFile('album_image')) {
            // $size= $request->file('banner_img')->getClientSize();
          
    
             $image = $request->album_image;
             $fileName = time() . '.' . $image->getClientOriginalExtension();
             $image->move('resources/assets/images/album_images/', $fileName);
             $uploadFile = 'resources/assets/images/album_images/' . $fileName;
         } else {
          
             $uploadFile = $request->album_image_old;
         }


    $album_update =
        DB::table('album_tbl')->where('album_id', $request->album_id)
                                 ->update([
                                    'album_id' =>$request->album_id,
                                    // 'album_title' => $request->album_title,
                                    'album_position' => $request->album_position,
                                    'album_image'=>$uploadFile

                                             ]);
    $message = "Record has been updated successfully";

    //return redirect()->back()->withErrors($message);
    return redirect()->route('album.listAlbum')->withErrors($message);

}
 
public function statusAlbum(Request $request)
    {
        DB::table('album_tbl')
            ->where('album_id', $request->id)
            ->update([
                'album_status'=>$request->status
            
            ]);



        $message = "Status has been changed successfully";
        return redirect()->back()->withErrors($message);
    }


    
public function deleteAlbum(Request $request)
{
    DB::table('album_tbl')
        ->where('album_id', $request->id)
        ->delete();



    $message = "Record has been deleted successfully";
    return redirect()->back()->withErrors($message);
}



public function addImg(Request $request)
{
    $result = array();
      $result['album_images']=  DB::table('album_image_tbl as tkbl')->orderBy('tkbl.sort', 'asc')->where('album_id',$request->id)->get();
      
       foreach ( $result['album_images'] as $i => $a) {
           $a->size = File::size($a->album_image_img);
             $a->name = File::name($a->album_image_img);
       }
    $result['data']=$request->id;
    return view("album.addImg")->with('result', $result);

}


public function uploadImg(Request $request)
    {
       
        $album_id=$request->album_id;
        $res= [];
        $album_im ;
        $sort = 0;
        $sortC = DB::table('album_image_tbl')->where('album_id',$album_id)->orderBy('sort','desc')->first();
       if($sortC){
           $sort = $sortC->sort;
       }
        	if($request->hasFile('file')){
 
            	$cars_pictures = $request->file;
            foreach ($cars_pictures as $i => $a) {
            $image = $a;
			$fileName = time().'.jpg';
			$image->move('resources/assets/images/album_images/all_image/', $fileName);
			$uploadImage = 'resources/assets/images/album_images/all_image/'.$fileName; 
			$car_im = DB::table('album_image_tbl')->insertGetId([
			    'album_id'=>$album_id,
			   	'album_image_img'=>$uploadImage,
			   	'sort'=>$sort+1
					]);	
            }
        }
   $images=  DB::table('album_image_tbl as tkbl')->orderBy('tkbl.sort', 'asc')->where('album_id',$album_id)->get();
   foreach ($images as $i => $a) {
           $a->size = File::size($a->album_image_img);
             $a->name = File::name($a->album_image_img);
       }
       return response()->json(['success'=>$images,'res'=>$res]);

    }


     
function deleteImg(Request $request)
    {
        
       $imgs=  DB::table('album_image_tbl')->where('album_image_id',$request->id)->get();
        //   foreach ( $imgs as $i => $a) {
      
        // $path=public_path().'/'.$a->album_image_img;
       // if (file_exists($path)) {
            // unlink($path);
        //}
        //   }
      DB::table('album_image_tbl')->where('album_image_id',$request->id)->delete();
            

        // $filename =  $request->get('filename');
        // ImageUpload::where('filename',$filename)->delete();
        // $path=public_path().'/images/'.$filename;
        // if (file_exists($path)) {
        //     unlink($path);
        // }
         return response()->json(['success'=>$path]);
        
    }

public function sortImages(Request $request){
	        $im = json_decode($request->data,true);
  foreach ($im['imgQ']  as $i => $a) {
     DB::table('album_image_tbl')->where('album_image_id',$a['id'])->update([
         'sort'=>$a['sort'] +1
         ]);
}
             return response()->json(['success'=>$im['imgQ']  ]);


	}
}
