<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Exception;
use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Models\Ourimg;
use App\Models\Services;
use App\Models\Video;
use Illuminate\Support\Facades\Hash;
use File;
class ServicesController extends Controller
{

public function listServices(){
    $listServices=Services::all();
    return view('services.listServices',compact('listServices'));
}

public function addServices(){
    return view('services.addServices');
}
public function addNewServices(Request $request){
    $service=new Services();
    $service->services_title=$request->input('services_title');
    $service->services_text=$request->input('services_text');
    $image = $request->services_picture;
    $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
    $image->move('uploaded/', $fileName);
    $uploadFile = 'uploaded/' . $fileName;
    $service->services_picture=$uploadFile;
    $service->services_design=$request->input('service_design');
    $slug=str_replace(" ","_",$request->input('services_title'));
    $service->services_slug=$slug;
    $service->save();
    return redirect()->route('services.listServices')->with('err','Record has been saved successfully');
    
}
public function editServices($id){
    $editServices=Services::find($id);
    return view('services.editServices',compact('editServices'));
}
public function updateServices(Request $request){
    $id=$request->input('services_id');
    $service=Services::find($id);
    $service->services_title=$request->input('services_title');
    $service->services_text=$request->input('services_text');
    if ($request->hasFile('services_picture')) {
        unlink($service->services_picture);
        $image = $request->services_picture;
        $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
        $image->move('uploaded/', $fileName);
        $uploadFile = 'uploaded/' . $fileName;
        }
        else
        {
        $uploadFile=  $service->services_picture;
        }
        $service->services_picture=$uploadFile;
        if($request->has('service_design')) $service->services_design=$request->input('service_design');
        $service->update();
        return redirect()->route('services.listServices')->with('err','Record has been updated successfully');
}
public function deleteServices($id){
    $service=Services::find($id);
    $service->delete();
    return redirect()->route('services.listServices')->with('err','Record has been deleted successfully');

}

public function addImg(Request $request)
{
    $result = array();
      $result['video_name']=  Video::all();
      
       foreach ( $result['video_name'] as $i => $a) {
       
           $a->size = File::size($a->video_name);
             $a->name = File::name($a->video_name);
       }

    return view('services.addImg')->with('result', $result);

}


public function uploadImg(Request $request)
    {
       
        // $our_id=$request->id;
        $res= [];
        // $album_im ;
    
        	if($request->hasFile('file')){
 
            	$cars_pictures = $request->file;
            foreach ($cars_pictures as $i => $a) {
            $image = $a;
			$fileName = time().rand(1000,50000).'.mp4';
			$image->move('uploaded/', $fileName);
			$uploadImage = 'uploaded/'.$fileName; 
			// $car_im =Ourimg::create([
			//    	'page1_image'=>$uploadImage,
			// 		]);	
            $img=new Video();
            $img->video_name=$uploadImage;
            $img->save();
            }
        }
       $images= Video::get();
       foreach ($images as $i => $a) {
               $a->size = File::size($a->video_name);
                 $a->name = File::name($a->video_name);
           }
           return response()->json(['success'=>$images,'res'=>$res]);

    }


     
function deleteImg(Request $request)
    {
        
       $imgs= Video::where('id',$request->id)->get();
          foreach ( $imgs as $i => $a) {
      
        $path=public_path().'/'.$a->video_name;
       if (file_exists($path)) {
            unlink($path);
        }
          }
        Video::where('id',$request->id)->delete();

         return response()->json(['success'=>'$path']);
        
    }

public function sortImages(Request $request){
	        $im = json_decode($request->data,true);
  foreach ($im['imgQ']  as $i => $a) {
    Ourimg::where('ourworkimages_id',$a['id'])->update([
         'ourworkimages_sort'=>$a['sort'] +1
         ]);
}
             return response()->json(['success'=>$im['imgQ']  ]);


	}
}
