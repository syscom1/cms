<?php

namespace App\Http\Controllers;

use App\Models\Our_story_banner;
use App\Models\Our_story_what;
use App\Models\Our_story_who;
use Illuminate\Http\Request;

class OurstoryController extends Controller
{
    public function listBanner()
{

    $listBanner = Our_story_banner::orderBy('id', 'desc')->get();

    return view('our_story.banner.listBanner')->with('listBanner', $listBanner);
}



public function addBanner(Request $request)
{
    $listBanner = Our_story_banner::orderBy('id','desc')->get();

    return view('our_story.banner.addBanner')->with('listBanner',$listBanner);
}

public function addNewBanner(Request $request)
{


    $bannerAdd=new Our_story_banner();
    $bannerAdd->title=$request->input('title');
         
        $image = $request->banner_img;
        $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
        $image->move('uploaded/', $fileName);
        $uploadFile = 'uploaded/' . $fileName;
    $bannerAdd->image=$uploadFile;
    $bannerAdd->save();
    $message = "Record has been added successfully";

    //return redirect()->route('banner.addBanner')->withErrors($message);
    return redirect()->route('story.listBanner')->withErrors($message);

}



public function editBanner(Request $request,$id)
{
    $editBanner = Our_story_banner::find($id);
 

    return view('our_story.banner.editBanner')->with('editBanner', $editBanner);
                                    
 
}


public function updateBanner(Request $request)
{
      


    $banner_update =Our_story_banner::find($request->id);
    $banner_update->title=$request->input('title');
    if($request->hasFile('banner_img')){
        unlink($banner_update->image);
        $image = $request->banner_img;
        $fileName = time().rand(1000,50000) . '.' . $image->getClientOriginalExtension();
        $image->move('uploaded/', $fileName);
        $uploadFile = 'uploaded/' . $fileName;
        $banner_update->image=$uploadFile;
    }
    $banner_update->update();
    $message = "Record has been updated successfully";
   return redirect()->route('story.listBanner')->withErrors($message);

}
 
public function statusBanner(Request $request,$id,$status)
    {
        $banner=Our_story_banner::find($id);
        $banner->status=$status;
        $banner->update();
        $message = "Status has been changed successfully";
        return redirect()->back()->withErrors($message);
    }






public function statuswa(Request $request,$id,$status)
    {
        $banner=Our_story_who::find($id);
        $banner->status=$status;
        $banner->update();
        $message = "Status has been changed successfully";
        return redirect()->back()->withErrors($message);
    }


    
public function deleteBanner(Request $request,$id)
{
    $banner=Our_story_banner::find($id);
    unlink($banner->image);
    $banner->delete();
    $message = "Record has been deleted successfully";
    return redirect()->back()->withErrors($message);
}



public function listwa(){
    $wa=Our_story_who::all();
    return view('our_story.wa.listWa',compact('wa'));
}

public function addwa(){
    return view('our_story.wa.addWa');
}

public function addNewWa(Request $request)
{

    $wa= new Our_story_who();
    $wa->title=$request->input('title');
    $wa->caption=$request->input('caption');
    $wa->save();
    $message = "Record has been added successfully";
    return redirect()->route('wa.list')->withErrors($message);
}

public function editWa($id){
    $wa=Our_story_who::find($id);
    return view('our_story.wa.editWa')->with('wa', $wa);

}

public function updateWa(Request $request){
$wa=Our_story_who::find($request->input('id'));
$wa->title=$request->input('title');
$wa->caption=$request->input('caption');
$wa->update();
$message = "Record has been updated successfully";
return redirect()->route('wa.list')->withErrors($message);

}

public function deleteWa($id){
    $wa=Our_story_who::find($id);
    $wa->delete();
    $message = "Record has been deleted successfully";
    return back()->withErrors($message);
}


public function listwm(){
    $wm=Our_story_what::all();
    return view('our_story.wm.listwm',compact('wm'));
}

public function addwm(){
    return view('our_story.wm.addwm');
}

public function addNewwm(Request $request)
{

    $wm= new Our_story_what();
    $wm->title=$request->input('title');
    $wm->caption=$request->input('caption');
    $wm->save();
    $message = "Record has been added successfully";
    return redirect()->route('wm.list')->withErrors($message);
}

public function editwm($id){
    $wm=Our_story_what::find($id);
    return view('our_story.wm.editwm')->with('wm', $wm);

}

public function updatewm(Request $request){
$wm=Our_story_what::find($request->input('id'));
$wm->title=$request->input('title');
$wm->caption=$request->input('caption');
$wm->update();
$message = "Record has been updated successfully";
return redirect()->route('wm.list')->withErrors($message);

}

public function deletewm($id){
    $wm=Our_story_what::find($id);
    $wm->delete();
    $message = "Record has been deleted successfully";
    return back()->withErrors($message);
}


public function statuswm(Request $request,$id,$status)
    {
        $banner=Our_story_what::find($id);
        $banner->status=$status;
        $banner->update();
        $message = "Status has been changed successfully";
        return redirect()->back()->withErrors($message);
    }

}
