<?php

namespace App\Http\Controllers;

use App\Models\Page1;
use Illuminate\Http\Request;
use DB;
use File;
use Collection;

class Page1Controller extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
 
        $editPage1 = Page1::first();

        return view('page1.editPage1')->with('editPage1',$editPage1);
    }
    
    
 
      public function update(Request $request,$id)
    {
        //dd($request->files);

        
        try{

 
        if ($request->hasFile('page1_image')) {
            // $size= $request->file('banner_img')->getClientSize();
       //   dd('image1');
    
             $image1 = $request->page1_image;
             $fileName1 = time() . '.' . $image1->getClientOriginalExtension();
             $image1->move('resources/assets/images/page1_image/', $fileName1);
             $uploadFile1 = 'resources/assets/images/page1_image/' . $fileName1;
         } else {

         //   dd('else1');
             //$size1='';
             $uploadFile1 = DB::table('page1_tbl')->where('page1_id','=',$request->id)->first()->page1_image;
         } 
         
    $storiesAdd = DB::table('page1_tbl')->where('page1_id','=',$request->id)->update([
      
                   	 
                          'page1_title'=>$request->page1_title,
                          'page1_text'=>$request->page1_text,
                          'page1_image'=>$uploadFile1
                          
                 
					]);

    $message = "Saved !";

        return redirect()->back()->withErrors($message);
                                           

  }
catch(Exception $e)
{
 

    $message = "Error!";

        return redirect()->back()->withErrors($message);

}
    }
    
}
