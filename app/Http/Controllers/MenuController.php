<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use DB;
use Exception;

class MenuController extends Controller
{
    // List Menu FUNCTIONS --------------------------------------------------------------->
    public function listMenu()
    {

        // $sort="asc";
        $listMenu=Menu::orderBy('position','asc')->get();
        // $listMenu = DB::table('menu_tbl')->orderBy('position',$sort)->get();

    return view('menu.listMenu')->with('listMenu', $listMenu);

        

    }

    public function addMenu(Request $request)
    {
        $s=array('s' => '');
        return view('menu.addMenu')->with($s);
    }


    public function addNewMenu(Request $request)
    {

try{
    $menuAdd=new Menu();
    $menuAdd->title=$request->input('title');
    $menuAdd->position=$request->input('position');
    $menuAdd->url=$request->input('url');
    $menuAdd->save();
    $message = "Record has been added successfully";
    return redirect()->route('menu.listMenu')->withErrors($message);
}
catch(Exception $e)
{
    $s=array('s' => '0');

    $message = "Cannot Duplicate Menu, This Name Exists !!";

    return view('menu.addMenu')->with($s)->withErrors($message);

}
        
    }


    public function editMenu(Request $request,$id)
    {
        $menu_id=$id;
        $editMenu=Menu::find($menu_id);

        return view('menu.editMenu')->with('editMenu', $editMenu);
     
    }

    public function updateMenu(Request $request)
    {
        $menu_update =Menu::find($request->input('menu_id'));
        $menu_update->title=$request->input('title');
        $menu_update->position=$request->input('position');
        $menu_update->url=$request->input('url');
        $menu_update->update();
        $message = "Record has been updated successfully";
        return redirect()->route('menu.listMenu')->withErrors($message);

        // return redirect()->back()->withErrors($message);
    }


   
    public function statusMenu(Request $request,$id,$status)
    {
        $menu=Menu::find($id);
        $menu->menu_status=$status;
        $menu->update();
        $message = "Status has been changed successfully";
        return redirect()->back()->withErrors($message);
    }

    public function deleteMenu(Request $request,$id)
{
    $menu=Menu::find($id);
    $menu->delete();
    $message = "Record has been deleted successfully";
    return redirect()->back()->withErrors($message);
}


}
