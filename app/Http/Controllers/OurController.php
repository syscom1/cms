<?php

namespace App\Http\Controllers;

use App\Models\Ourwork;
use App\Models\OurworkImages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Laravel\Ui\Presets\React;

class OurController extends Controller
{
    public function listOur(){
        $our=Ourwork::all();
        return view('ourwork.listOur',compact('our'));
    }
    public function addOur(){
        return view('ourwork.addOur');
    }
    public function addNewOur(Request $request){
        $tags=json_encode($request->input('our_tags'));
        // $tags=json_decode($tags);
        $our=new Ourwork();
        $our->our_title=$request->input('our_title');
        $our->our_caption=$request->input('our_caption');
        $our->our_tags=$tags;
        $our->save();
        return redirect()->route('ourwork.listOur')->with('err','Record has been saved successfully.');
    }
    public function editOur($id){
        $our=Ourwork::find($id);
        return view('ourwork.editOur',compact('our'));
    }

    public function updateOur(Request $request){
        $id=$request->input('id');
        $our=Ourwork::find($id);
        $our->our_title=$request->input('our_title');
        $our->our_caption=$request->input('our_caption');
        $tags=json_encode($request->input('our_tags'));
        $our->our_tags=$tags;
        $our->update();
        return redirect()->route('ourwork.listOur')->with('err','Record has been updated successfully.');
    }

    public function deleteOur($id){
        $our=Ourwork::find($id);
        $our->delete();
        return redirect()->route('ourwork.listOur')->with('err','Record has been deleted successfully.');
    }

public function addImg(Request $request)
{
    $result = array();
      $result['album_images']=  OurworkImages::where('our_id',$request->id)->orderBy('ourworkimages_sort', 'asc')->get();
      
       foreach ( $result['album_images'] as $i => $a) {
       
           $a->size = File::size($a->ourworkimages_image);
             $a->name = File::name($a->ourworkimages_image);
       }
    $result['data']=$request->id;
 

    return view("album.addImg")->with('result', $result);

}


public function uploadImg(Request $request)
    {
       
        $our_id=$request->our_id;
        $res= [];
        // $album_im ;
        $sort = 0;
        $sortC = OurworkImages::where('our_id',$our_id)->orderBy('ourworkimages_sort','desc')->first();
       if($sortC){
           $sort = $sortC->sort;
       }
        	if($request->hasFile('file')){
 
            	$cars_pictures = $request->file;
            foreach ($cars_pictures as $i => $a) {
            $image = $a;
			$fileName = time().rand(1000,50000).'.jpg';
			$image->move('uploaded/', $fileName);
			$uploadImage = 'uploaded/'.$fileName; 
			$car_im =OurworkImages::create([
			    'our_id'=>$our_id,
			   	'ourworkimages_image'=>$uploadImage,
			   	'ourworkimages_sort'=>$sort+1
					]);	
            }
        }
   $images= OurworkImages::orderBy('ourworkimages_sort', 'asc')->where('our_id',$our_id)->get();
   foreach ($images as $i => $a) {
           $a->size = File::size($a->ourworkimages_image);
             $a->name = File::name($a->ourworkimages_image);
       }
       return response()->json(['success'=>$images,'res'=>$res]);

    }


     
function deleteImg(Request $request)
    {
        
    //    $imgs= OurworkImages::where('ourworkimages_id',$request->id)->get();
        //   foreach ( $imgs as $i => $a) {
      
        // $path=public_path().'/'.$a->album_image_img;
       // if (file_exists($path)) {
            // unlink($path);
        //}
        //   }
        OurworkImages::where('ourworkimages_id',$request->id)->delete();
            

        // $filename =  $request->get('filename');
        // ImageUpload::where('filename',$filename)->delete();
        // $path=public_path().'/images/'.$filename;
        // if (file_exists($path)) {
        //     unlink($path);
        // }
         return response()->json(['success'=>'$path']);
        
    }

public function sortImages(Request $request){
	        $im = json_decode($request->data,true);
  foreach ($im['imgQ']  as $i => $a) {
    OurworkImages::where('ourworkimages_id',$a['id'])->update([
         'ourworkimages_sort'=>$a['sort'] +1
         ]);
}
             return response()->json(['success'=>$im['imgQ']  ]);


	}
}
