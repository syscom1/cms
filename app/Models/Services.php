<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    use HasFactory;
    protected $table='services_tbl';
    protected $fillable=['id','services_title','services_text','services_picture','services_serivces','services_design','services_slug'];
}
