<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Our_story_banner extends Model
{
    use HasFactory;
    protected $table='our_story_banner';
    protected $fillable=['title','image','banner_status'];
}
