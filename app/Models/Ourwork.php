<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ourwork extends Model
{
    use HasFactory;
    protected $table='ourwork_tbl';
    protected $primaryKey='our_id';
    protected $fillable=['our_id','our_title','our_caption','our_tags','our_images'];


    public function images()
    {
        return $this->hasMany(OurworkImages::class,'our_id','our_id');
    }
}
