<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OurworkImages extends Model
{
    use HasFactory;
    protected $table='ourworkimages_tbl';
    protected $primaryKey='ourworkimages_id';
    protected $fillable=['ourworkimages_id','our_id','ourworkimages_image','ourworkimages_sort'];
}
