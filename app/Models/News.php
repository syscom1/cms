<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;
    protected $table='news_tbl';
    protected $primaryKey='news_id';

    protected $fillable=['news_id','news_title','news_caption','news_image','news_date'];
}
