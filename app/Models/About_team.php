<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About_team extends Model
{
    use HasFactory;
    protected $table='about_team_tbl';
    protected $fillable=['id','team_name','team_caption','team_image'];
}
