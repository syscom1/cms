<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $table='category_details';
    protected $primaryKey='category_id';
    protected $fillable=['banner_photo','name','banner_link','cover_photo','cover_title','cover_text'];

    public function subcategory(){
        return $this->hasMany(Subategory::class,'category_id');
    }
}
