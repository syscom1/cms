<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category_article extends Model
{
    use HasFactory;
    protected $table='category_article_tbl';
    protected $primaryKey='category_article_id';
    protected $fillable=['category_id','article_id'];
    
    
    public $timestamps=false;
    public function category(){
        return $this->hasMany(Category::class,'category_id','category_id');
    }
}
