<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Infos1 extends Model
{
    use HasFactory;
    protected $table="info1_tbl";
    protected $primaryKey='info1_id';
    protected $fillable=['info1_about_us','info1_about_us_fr','info1_image','sort'];
    public $timestamps=false;
}
