<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    use HasFactory;
    protected $table="info_tbl";
    protected $primaryKey='info_id';
    protected $fillable=['info_about_us','info_about_us_fr','info_image','sort'];
    public $timestamps=false;
}
