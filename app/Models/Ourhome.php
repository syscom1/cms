<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ourhome extends Model
{
    use HasFactory;
    protected $table='page1_tbl';
    protected $fillable=['id','page1_title','page1_text'];
}
