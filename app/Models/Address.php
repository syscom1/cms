<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;
    protected $table='address_tbl';
    protected $primaryKey='address_id';
    protected $fillable=['address_id','address_street','address_building','address_floor','address_apartment_number','address_special_instruction','address_latitude','address_longitude','address_added_date','address_modified_date'];

    public function address_user(){
        return $this->hasMany(Address_user::class,'address_id','address_id');
    }
}
