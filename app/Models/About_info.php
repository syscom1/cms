<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About_info extends Model
{
    use HasFactory;
    protected $table='about_info_tbl';
    protected $fillable=['id','info_caption'];
}
