<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address_user extends Model
{
    use HasFactory;
    protected $table='address_user_tbl';
    protected $fillable=['address_user_id','address_id','user_id','address_user_added_date','address_user_modified_date'];
    public function user(){
        return $this->hasMany(Users::class,'user_id','user_id');
    }
}
