<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Catimage extends Model
{
    use HasFactory;
    protected $table="category_image";
    protected $fillable=['category_id','image'];
}
