<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    protected $table='menu_tbl';
    protected $primaryKey = 'menu_id';
    protected $fillable=['title','position','slug','menu_status','url'];
    public $timestamps=false;
}
