<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page1 extends Model
{
    use HasFactory;
    protected $table='page1_tbl';
    protected $fillable=['page1_title','page1_text','page1_status'];
    public $timestamps=false;
}
