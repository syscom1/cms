<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ourimg extends Model
{
    use HasFactory;
    protected $table='page1_image';
    protected $fillable=['page1_image'];
}
