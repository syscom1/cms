<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Our_story_what extends Model
{
    use HasFactory;
    protected $table='our_story_what';
    protected $fillable=['title','caption'];
}
