<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subimage extends Model
{
    use HasFactory;
    protected $table="subcategory_image";
    protected $fillable=['subcategory_id','image'];
}
