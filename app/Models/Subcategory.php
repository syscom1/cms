<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Subcategory extends Model
{
    use HasFactory;
    protected $table='subcategory_details';
    protected $fillable=['category_id','banner_photo','banner_title','banner_link','cover_photo','cover_title','cover_text','category_status'];

    public function category(){
        return $this->hasMany(Category::class);
    }
}
