<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;
    protected $table='contact_tbl';
    protected $primaryKey='contact_id';
    protected $fillable=['contact_address','contact_phone','contact_email','contact_facebook','contact_instagram','contact_twitter','contact_status','fax','image'];
    public $timestamps=false;
}
