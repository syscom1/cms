<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;
    protected $table='banner_tbl';
    protected $primaryKey='banner_id';
    protected $fillable=['banner_img','banner_url','title','caption','title_fr','caption_fr','record_position','banner_status','banner_expired_date','banner_added_date','banner_modified_date'];
    public $timestamps=false;
}
