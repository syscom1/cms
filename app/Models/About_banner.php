<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class About_banner extends Model
{
    use HasFactory;
    protected $table='about_banner_tbl';
    protected $fillable=['id','banner_title','banner_caption'];
}
