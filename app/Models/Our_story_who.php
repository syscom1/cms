<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Our_story_who extends Model
{
    use HasFactory;
    protected $table='our_story_who';
    protected $fillable=['title','caption'];
}
